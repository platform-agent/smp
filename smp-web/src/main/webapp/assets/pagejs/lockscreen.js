jQuery(document).ready(function ($) {
    setTimeout(function () {
        $(".fade-in-effect").addClass('in');
    }, 1);


    // Clicking on thumbnail will focus on password field
    $(".user-thumb a").on('click', function (ev) {
        ev.preventDefault();
        $("#userPassWord").focus();
    });

    // Validation and Ajax action
    $(".lockcreen-form").validate({
        rules: {
            userPassWord: {
                required: true,
                minlength: 6
            }
        },
        messages: {
            userPassWord: {
                required: '请输入您的密码.',
                minlength: '密码不能小于{0}个字符'
            }
        },

        // Form Processing via AJAX
        submitHandler: function (form) {
            var butelement = $(".btn-primary");
            butelement.buttonLoading();//按钮锁定

            var formdata = $("#lockscreen").serializeObject();
            var url = contextPath + "/user-login/login";
            AjaxPlugin.sendDataRequest(url , formdata ,'POST' , function(resp){
                butelement.buttonReset();
                if (resp.success) {
                    window.location.href = contextPath + '/portal/home';
                }
                else {
                    var error = resp.messageBeans[0].message;
                    MessageTool.init(MessageTool.position.full.top)
                    MessageTool.error(error);
                    $(form).find('#userPassWord').select();
                }
            });
        }
    });
    // Set Form focus
    $("form#lockscreen .form-group:has(.form-control):first .form-control").focus();
});