/**
 * UI 扩展工具封装
 * Created by SIVEN on 17/4/24.
 */

/**
 * 进度条工具
 */
var ProgressBarTool = {
    //进度条开始
    start: function (num) {
        if (num) num = 70; // Fill progress bar to 70% (just a given value)
        show_loading_bar(num);
    },
    //结束
    finish: function (callback) {
        show_loading_bar({
            delay: .5,
            pct: 100,
            finish: function () {
                callback;
            }
        });
    },
    //回滚
    back: function () {
        show_loading_bar(0);
    }
}

/**
 * Message 工具
 * 封装toastr提供页面使用
 */

var MessageTool = {

        //显示的位置
        position: {

            default: this.top.right,

            full: {
                top: 'toast-top-full-width',
                bottom: 'toast-bottom-full-width'
            },

            top: {
                left: 'toast-top-left',
                right: 'toast-top-right'
            },

            bottom: {
                left: 'toast-bottom-left',
                right: 'toast-bottom-right',
            }
        },

        //默认显示参数
        defaultOpts: {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        },

        //初始化参数, 传入opts对象, 单独传入某个数组
        init: function (param) {
            if (typeof param == "object") {
                for (var key in param) {
                    this.defaultOpts[key] = param[key];
                }
            } else {
                if (param) {
                    this.defaultOpts.positionClass = param;
                } else {
                    //为空时, 样式默认
                    this.defaultOpts.positionClass = this.position.default;
                }
            }

        },

        //成功
        success: function (message, title) {
            toastr.success(message, title, this.defaultOpts);
        },

        //错误
        error: function (message, title) {
            toastr.error(message, title, this.defaultOpts);
        },

        //警告
        warning: function (message, title) {
            toastr.warning(message, title, this.defaultOpts);
        },

        //提示
        info: function (message) {
            toastr.info(message);
        }
    }


/*--------------------------------------bootstrap 扩展--------------------------------------*/
;(function ($) {

    /**
     *  data-loading-text="loading..."
     * 设置按钮提交时显示文本
     * @param text
     * @constructor
     */
    $.fn.buttonLoading = function (text) {
        if (!text) text = $(this).html();
        $(this).button.Constructor.DEFAULTS.loadingText = text;
        $(this).button('loading');
    };

    /**
     * 设置按钮提交时显示文本, 并自动移除
     * @param text
     * @constructor
     */
    $.fn.buttonLoadingTimeOut = function (outTime) {
        if (!outTime) outTime = 2000;
        var button = $(this);
        button.buttonLoading();
        setTimeout(function(){
            button.buttonReset();
        } , outTime);
    };

    $.fn.buttonLoading = function (text) {
        if (!text) text = $(this).html();
        $(this).button.Constructor.DEFAULTS.loadingText = text;
        $(this).button('loading');
    };

    /**
     * 按钮重置
     */
    $.fn.buttonReset = function () {
        $(this).button('reset');
    };

})(jQuery);
/*--------------------------------------bootstrap 扩展结束--------------------------------------*/