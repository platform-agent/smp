/**
 * bootstrap table formatter 通用组件
 * @type {{status: formatter.status}}
 */
var formatter = {
    /**
     * 状态转换
     * @param value
     * @param row
     * @param index
     * @returns {*}
     */
    status: function (value) {
        if (value == 'Y') {

            return '<div class="label label-secondary">有效</div>';
        }
        return '<div class="label label-red">失效</div>';
    },
    /**
     * 日期格式转换 yyyy-MM-dd hh:mm:ss
     * @param value
     * @param row
     * @param index
     * @returns {string}
     */
    date: function (value) {
        if (null == value) return "";
        var date = new Date();
        date.setTime(value);
        return date.Format("yyyy-MM-dd hh:mm:ss");
    },
    /**
     * 日期格式转换 yyyy-MM-dd
     * @param value
     * @param row
     * @param index
     * @returns {string}
     */
    date2: function (value) {
        if (null == value) return "";
        var date = new Date();
        date.setTime(value);
        return date.Format("yyyy-MM-dd");
    }
};