/**
 * @Title: 通用的Ajax组件
 * @依赖插件: MessageTool , ProgressBarTool  common-tools.js
 */
var AjaxPlugin = {
    MessageTool: MessageTool,
    ProgressBarTool: ProgressBarTool,

    //初始化插件
    Init: function (messagePlugin, progressBarPlugin) {
        this.MessageTool = messagePlugin;
        this.ProgressBarTool = progressBarPlugin;
    },

    /**
     * 简单的Ajax数据请求
     * @param url
     * @param data
     * @param type
     * @param success_callback
     */
    sendDataRequest: function (url, data, type, callback) {
        this.ProgressBarTool.start();
        $.ajax({
            type: type,
            url: url,
            contentType: 'application/json;charset=UTF-8',
            data: JSON.stringify(data),
            dataType: "json",
            success: function (result) {
                if (callback) callback(result);
                else {
                    if (result.success) {
                        AjaxPlugin.MessageTool.success("处理成功");
                    } else {
                        var message = result.messageBeans[0].message;
                        AjaxPlugin.MessageTool.error("处理失败, " + message);
                    }
                }
                AjaxPlugin.ProgressBarTool.finish(callback);
            }, error: function (message) {
                AjaxPlugin.exception(message);
            }
        });
    },
    /**
     * 发送简单的Ajax请求
     * @param url
     * @param data
     * @param type
     * @param callback
     */
    sendSimpleAjaxRequest: function (url, data, type, async, callback) {
        $.ajax({
            type: type,
            url: url,
            async: async,
            contentType: 'application/json;charset=UTF-8',
            data: JSON.stringify(data),
            dataType: "json",
            success: function (result) {
                if (callback) callback(result);
            }, error: function (message) {
                AjaxPlugin.exception(message);
            }
        });
    },

    /**
     * 封装弹出确认框, 点击确认后并发送Ajax请求
     * @param title 标题
     * @param content 内容
     * @param buttonName 按钮名称
     * @param url 请求的URL
     * @param data 请求的数据
     * @param callback 回调函数
     * @constructor
     */
    Confirm: function (title, content, buttonName, url, data, callback) {
        buttonName = buttonName.replace(/ /g, '');
        Confirm.show(title, content, {
            '确认': {
                'primary': true,
                'buttonName': buttonName,
                'callback': function () {
                    AjaxPlugin.sendDataRequest(url, data, 'POST', function (result) {
                        if (result.success) {
                            MessageTool.success(buttonName + "成功");
                            Confirm.hide();
                            if (callback) callback(result);
                        } else {
                            MessageTool.error(buttonName + "失败");
                        }
                    });
                }
            }
        });
    },
    //系统异常消息提示
    exception: function (message) {
        AjaxPlugin.ProgressBarTool.finish(function () {
            AjaxPlugin.MessageTool.error("系统异常! " + message);
        });
    }
};

;(function ($) {

    /**
     * 绑定点击事件提交表单
     * @param form form表单[FROMID名称或FROM对象]
     * @param url URL
     * @param type [POST , GET]
     * @param callback 回调函数
     * 示例:
     ----------------------------------------
     var form = $("#user-edit");
     var url = contextPath + "/user/save";
     //无回调函数
     $(".btn-success").bindClickEvent4SubmitFrom(form, url, "POST");

     //自定义回调函数版
     $(".btn-success").bindClickEvent4SubmitFrom("formID", url, "POST",
     function (result) {
            if (result.success) {
                toastrMsg.success("", "提交成功");
            } else {
                var message = result.messageBeans[0].message;
                toastrMsg.error("", "提交失败: " + message);
            }
         }
     );
     ----------------------------------------
     */
    $.fn.bindClickEvent4SubmitFrom = function (form, url, type, callback) {
        var formObj = $(form);
        this.click(function () {
            //校验表单
            if (!formObj.valid())return;

            var element = $(this);
            //element.addClass("disabled");
            element.buttonLoading();//按钮锁定

            var formdata = formObj.serializeJSONStr();

            AjaxPlugin.ProgressBarTool.start();
            $.ajax({
                type: type,
                url: url,
                contentType: 'application/json;charset=UTF-8',
                data: formdata,
                dataType: "json",
                success: function (result) {
                    if (result.success) {
                        AjaxPlugin.MessageTool.success("提交成功");
                        if (callback) callback(result);
                    } else {
                        var message = result.messageBeans[0].message;
                        AjaxPlugin.MessageTool.error("提交失败, " + message);
                    }
                    AjaxPlugin.ProgressBarTool.finish(callback);
                    //element.removeClass("disabled");
                    element.buttonReset();
                }, error: function (message) {
                    element.buttonReset();
                    AjaxPlugin.exception(message);
                }
            });
        });
    };

    /**
     * 设置FORM表单值
     * @param url
     * @param data
     * @param type
     * @param success_callback
     */
    $.fn.formSetData = function (url, data, type, callback) {
        var formObj = $(this);
        AjaxPlugin.ProgressBarTool.start();
        $.ajax({
            type: type,
            url: url,
            contentType: 'application/json;charset=UTF-8',
            data: JSON.stringify(data),
            dataType: "json",
            success: function (result) {
                if (result.success) {
                    formObj.resetForm();//先清空表单
                    formObj.autofill(result.resultData);//设值
                    if (callback) callback(result);
                } else {
                    var message = result.messageBeans[0].message;
                    AjaxPlugin.MessageTool.error("获取数据失败, " + message);
                }
                AjaxPlugin.ProgressBarTool.finish(callback);
            }, error: function (message) {
                AjaxPlugin.exception(message);
            }
        });
    };

    /**
     * 封装 简单的bootstrapTable方法
     * @param searchform
     * @param url
     * @param columns
     * @constructor
     */
    $.fn.SimpleTableInit = function (searchform, url, columns) {
        $(this).bootstrapTable({
            url: url,         //请求后台的URL（*）
            method: 'POST',                      //请求方式（*）
            toolbar: '#toolbar',                //工具按钮用哪个容器
            striped: true,                      //是否显示行间隔色
            cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
            pagination: true,                   //是否显示分页（*）
            sortable: false,                     //是否启用排序
            sortOrder: "asc",                   //排序方式
            showExport: true,                     //是否显示导出
            exportDataType: "basic",              //basic', 'all', 'selected'.
            exportTypes: ['xml', 'csv', 'txt', 'excel'],
            queryParams: queryParams,//传递参数（*）
            responseHandler: responseHandler, //加载服务器数据之前的处理程序，可以用来格式化数据。参数：res为从服务器请求到的数据
            sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
            pageNumber: 1,                       //初始化加载第一页，默认第一页
            pageSize: 10,                       //每页的记录行数（*）
            pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
            search: true,                       //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
            searchOnEnterKey:true,              //设置为 true时，按回车触发搜索方法，否则自动触发搜索方法
            strictSearch: true,
            showColumns: true,                  //是否显示所有的列
            showRefresh: true,                  //是否显示刷新按钮
            minimumCountColumns: 1,             //最少允许的列数
            clickToSelect: true,                //是否启用点击选中行
            height: 600,                        //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
            uniqueId: "uuid",                     //每一行的唯一标识，一般为主键列
            showToggle: true,                    //是否显示详细视图和列表视图的切换按钮
            cardView: false,                    //是否显示详细视图
            detailView: false,                   //是否显示父子表
            columns: columns,
            onLoadError: function () {  //加载失败时执行
                AjaxPlugin.ProgressBarTool.finish();
                this.MessageTool.error("加载数据失败");
            }, onLoadSuccess: function () {  //加载失败时执行
                AjaxPlugin.ProgressBarTool.finish();
            }
        });

        //查询条件
        function queryParams(params) {
            AjaxPlugin.ProgressBarTool.start();
            var searchParams = {};
            searchParams.params = $(searchform).serializeObject();
            searchParams.pageSize = params.pageSize;
            searchParams.page = params.pageNumber;
            searchParams.params.search = params.search;

            JSON.stringify(searchParams)

            return JSON.stringify(searchParams);
        }

        //返回结果
        function responseHandler(res) {
            if (res.success) {
                return res.resultData;
            } else {
                this.MessageTool.error("加载数据失败");
            }
        }
    };

    /**
     * 刷新
     */
    $.fn.tableRefresh = function () {
        $(this).bootstrapTable('refresh');
    };

    /**
     * 加载树结构
     * @param url url
     * @param data 请求参数
     * @param selected_callback 选择树节点回调函数
     */
    $.fn.loadTreeview = function (url, data, selected_callback) {

        var treeObj = $(this);
        var treeSelect = treeObj.treeview('getSelected');
        var lastClick = treeSelect[0].text;//上次点击的

        AjaxPlugin.sendSimpleAjaxRequest(url, data, "POST", true, function (result) {
            var resultData = result.resultData;
            treeObj.treeview({
                expandIcon: 'fa-folder-o',
                collapseIcon: 'fa-folder-open-o',
                enable: true,
                data: resultData,
                onNodeSelected: function (event, data) {
                    selected_callback(data);
                }, error: function () {
                    this.MessageTool.error("加载数据失败");
                }
            });
            //$('#tree').treeview('collapseAll');

            if (lastClick == undefined) {
                treeObj.treeview('expandAll', {levels: 1, silent: true});//收起节点, 1节点除外
                treeObj.treeview('selectNode', [0, {silent: true}]);//选中1节点
            } else {
                var findSelectableNodes = function () {
                    var findSelectableNodes = treeObj.treeview('search', [lastClick, {
                        ignoreCase: false,
                        exactMatch: true,
                        revealResults: false
                    }]);
                    treeObj.treeview('clearSearch');
                    return findSelectableNodes
                };

                treeObj.treeview('selectNode', [findSelectableNodes(), {silent: true}]);
                treeObj.treeview('revealNode', [findSelectableNodes(), {silent: true}]);//展开匹配结果的节点
            }
        });
    };

    /**
     * 绑定点击事件加载树
     * @param url url
     * @param data 请求参数
     * @param selected_callback 选择树节点回调函数
     */
    $.fn.bindClickShowTreeview = function (url, data, idInput) {
        var textInput = $(this);
        var idInput = $(idInput);

        var treeDivId = textInput.id + "_tree_div";
        var treeId = textInput.id + "_treeview";
        var searchTreeId = textInput.id + "_searchTreeInput";
        textInput.after('<div style="display: none;" id="' + treeDivId + '"><input type="text" class="form-control" id="' + searchTreeId + '" placeholder="搜索" ><div id="' + treeId + '"></div></div>');
        var treeDiv = $("#" + treeDivId);
        var tree = $("#" + treeId);
        var searchInput = $("#" + searchTreeId);

        textInput.click(function () {
            treeDiv.toggle();
            if (!treeDiv.is(":hidden")) {
                AjaxPlugin.sendSimpleAjaxRequest(url, data, "POST", true, function (result) {
                    var resultData = result.resultData;
                    tree.treeview({
                        expandIcon: 'fa-folder-o',
                        collapseIcon: 'fa-folder-open-o',
                        enable: true,
                        data: resultData,
                        onNodeSelected: function (event, data) {
                            textInput.val(data.text);
                            idInput.val(data.id);
                            treeDiv.hide();

                        }, error: function () {
                            this.MessageTool.error("加载数据失败");
                        }
                    });
                    //绑定树搜索事件
                    searchInput.bindTreeSearch(tree);

                    var findSelectableNodes = function () {
                        var findSelectableNodes = tree.treeview('search', [textInput.val(), {
                            ignoreCase: false,
                            exactMatch: true,
                            revealResults: false
                        }]);
                        tree.treeview('clearSearch');
                        return findSelectableNodes
                    };

                    tree.treeview('selectNode', [findSelectableNodes(), {silent: true}]);
                    tree.treeview('revealNode', [findSelectableNodes(), {silent: true}]);//展开匹配结果的节点

                    //$('#tree').treeview('collapseAll');
                    //tree.treeview('expandAll', {levels: 1, silent: true});//收起节点, 1节点除外
                    // tree.treeview('expandAll');//收起节点, 1节点除外
                    //tree.treeview('selectNode', [0, {silent: true}]);//选中1节点
                });
            }
        });

        /**
         * 鼠标移开触发事件
         */
        textInput.parent().parent().mouseleave(function () {
            treeDiv.hide();
        });
    };

    /**
     * 为输入框绑定搜索树数据操作(模糊)
     */
    $.fn.bindTreeSearch = function (treeObj) {
        var searchInput = $(this);
        var search = function () {
            var pattern = searchInput.val();
            var options = {
                ignoreCase: true
            };
            $(treeObj).treeview('search', [pattern, options]);
        }
        searchInput.on('keyup', search);
    };
})(jQuery);