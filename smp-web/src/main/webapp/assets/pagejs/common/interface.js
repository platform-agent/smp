var INTERFACE = {

    /**
     * 获取当前用户信息
     */
    getCurrentUserInfo : function(sync){
        var resultData = null;
        $.ajax({
            async : sync,
            type: "POST",
            url: contextPath + "/portal/get-current-userinfo",
            contentType: 'application/json;charset=UTF-8',
            dataType: "json",
            success: function (result) {
                resultData = result.resultData;
                if (!result.success) {
                    var message = result.messageBeans[0].message;
                    MessageTool.init();
                    MessageTool.error("获取用户信息失败: " + message);
                }
            }, error: function (message) {
                MessageTool.init();
                MessageTool.error("获取用户信息异常! " + message);
            }
        });
        return resultData;
    },

    /**
     * 获取所有部门分组集合
     * @param async
     * @param callback
     */
    getAllGroupList : function (async, callback){

    }
}