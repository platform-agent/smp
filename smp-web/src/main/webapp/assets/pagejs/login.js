jQuery(document).ready(function ($) {
    // Reveal Login form
    setTimeout(function () {
        $(".fade-in-effect").addClass('in');
    }, 1);


    // Validation and Ajax action
    $("#login").validate({
        rules: {
            account: {
                required: true
            },

            password: {
                required: true,
                minlength: 6
            }
        },

        messages: {
            account: {
                required: '请输入您的用户名'
            },

            password: {
                required: '请输入您的密码',
                minlength: '密码不能小于{0}个字符'
            }
        },

        // Form Processing via AJAX
        submitHandler: function (form) {

            var butelement = $(".btn-primary");
            butelement.buttonLoading();//按钮锁定
            var formdata = $("#login").serializeObject();
            var url = contextPath + "/user-login/login";
            AjaxPlugin.sendDataRequest(url , formdata ,'POST' , function(resp){
                butelement.buttonReset();
                if (resp.success) {
                    window.location.href = contextPath + '/portal/home';
                }else {
                    // Remove any alert
                    $(".errors-container .alert").fadeOut("slow");

                    var error = resp.messageBeans[0].message;
                    var code = resp.messageBeans[0].code;
                    $(".errors-container").html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' + error + '</div>');
                    $(".errors-container .alert").hide().fadeIn();

                    $(form).find('#account').select();
                    if (code == 'account.password.isnot.consistent') {
                        $(form).find('#password').select();
                    }
                }
            });
        }
    });

    // Set Form focus
    $("form#login .form-group:has(.form-control):first .form-control").focus();
});