document.write("<script src='" + contextPath + "/assets/pagejs/common/interface.js' type='text/javascript'></script>");

var userinfo;//用户信息
jQuery(document).ready(function () {
    userinfo = INTERFACE.getCurrentUserInfo(false);
    welcomeToastr();

    //2.初始化Button的点击事件
    var oButtonInit = new ButtonInit();
    oButtonInit.Init();
});

//按钮初始化
var ButtonInit = function () {
    var oInit = new Object();

    oInit.Init = function () {
        //设置用户信息 - 用户名称
        $("#user-details-name").text(userinfo.name)

        //密码修改
        $("#chang-password").click(function () {
            $("#change-password-form").resetForm();
            $("#change-password-modal").modal('show');
        });

        //iframe地址为登录时, 主框架页面跳转到登录界面
        $("#content").load(function () {
            /*
            try {
                var contentHref = document.getElementById("content").contentWindow.location.href;
            }catch (e){
                MessageTool.init(MessageTool.position.bottom.left);//初始化现实位置
                MessageTool.warning("您的登录已经失效, 请重新登录!");
                setTimeout(function () {
                    window.location.href = contentHref;
                }, 2500);
            }
            */
            var contentHref = document.getElementById("content").contentWindow.location.href;
            if (contentHref.indexOf('/sso') > -1) {
                MessageTool.init(MessageTool.position.bottom.left);//初始化现实位置
                MessageTool.warning("您的登录已经失效, 请重新登录!");
                setTimeout(function () {
                    window.location.href = contentHref;
                }, 2500)
            }
        });

        // Validation and Ajax action
        $("#change-password-form").validate({
            rules: {
                oldPassword: {
                    required: true,
                    minlength: 6
                },

                newPassword: {
                    required: true,
                    minlength: 6
                },

                confirmPassword: {
                    required: true,
                    minlength: 6,
                    equalTo: "#newPassword"
                }
            },

            messages: {
                oldPassword: {
                    required: '请输入您的旧密码',
                    minlength: '密码不能小于{0}个字符'
                },

                newPassword: {
                    required: '请输入您的新密码',
                    minlength: '密码不能小于{0}个字符'
                },

                confirmPassword: {
                    required: '请输入您的确认密码',
                    minlength: '密码不能小于{0}个字符',
                    equalTo: '两次密码输入不一致'
                }
            },
            errorElement: 'span',
            errorClass: 'validate-has-error',
            // Form Processing via AJAX
            submitHandler: function (form) {
                //show_loading_bar(70); // Fill progress bar to 70% (just a given value)
                ProgressBarTool.start();

                var element = $("#change-password-button");
                //改变默认提交时, 显示的值
                element.buttonLoading();
                
                var formdata = $("#change-password-form").serializeJSONStr();
                $.ajax({
                    type: "POST",
                    contentType: 'application/json;charset=UTF-8',
                    url: contextPath + "/user/change-password",
                    data: formdata,
                    dataType: 'json',
                    success: function (result) {
                        MessageTool.init();
                        if (result.success) {
                            MessageTool.success("密码修改成功");
                            $("#change-password-modal").modal('hide');
                        } else {
                            var message = result.messageBeans[0].message;
                            MessageTool.error("密码修改失败, " + message);
                        }
                        element.buttonReset();
                        ProgressBarTool.finish();
                    }, error: function (message) {
                        ProgressBarTool.finish(function () {
                            MessageTool.error("系统异常! " + message);
                            element.buttonReset();
                        });
                    }
                });

            }
        });
    }
    return oInit;
}

//欢迎光临提示信息
function welcomeToastr() {
    setTimeout(function () {
        MessageTool.info("欢迎回来," + userinfo.name);
    }, 1000);
}

function mainJumpPage(url){
    alert("!!");
    window.location.href = url;
}
