document.write("<script src='" + contextPath + "/assets/pagejs/common/interface.js' type='text/javascript'></script>");
jQuery(document).ready(function ($) {
    var oInit = new Initialize();
    oInit.pageInit();
    oInit.pluginInit();
});

/**
 * 初始化
 * @returns {Object}
 * @constructor
 */
var Initialize = function () {
    var oInit = new Object();

    /**
     * 页面公共资源初始化
     */
    oInit.pageInit = function () {
        var $searchHeight = $(".panel-heading").height()
        var $pageHeight;
        //注意：下面的代码是放在test.html调用
        $(window.parent.document).find("#content").load(function () {
            try {
                var main = $(window.parent.document).find("#content");
                $pageHeight = $(document).height();
                main.height($pageHeight);
            } catch (e) {
            }
        });

        $("body").click(function () {
            $(window.parent.document).find("#main-menu").removeClass("mobile-is-visible");
            $(window.parent.document).find(".user-info-menu > .open").removeClass("open");
        });


        //panal -
        $(".collapse-icon").click(function () {
            setTimeout(function () {
                var main = $(window.parent.document).find("#content");
                main.height($pageHeight - $searchHeight + 10);
            }, 100);
        });

        //panal +
        $(".expand-icon").click(function () {
            setTimeout(function () {
                var main = $(window.parent.document).find("#content");
                var thisheight = $(document).height() - $searchHeight;
                main.height(thisheight);
            }, 100);
        });
    }

    /**
     * 初始化页面插件
     */
    oInit.pluginInit = function () {
        try {
            MessageTool = window.parent.MessageTool;
            ProgressBarTool = window.parent.ProgressBarTool;
            AjaxPlugin.Init(MessageTool, ProgressBarTool);
        } catch (e) {}
    }

    return oInit;

}

//展示Main模式窗口
function showMainModal() {
    $(window.parent.document).find("#main-modal").modal('show');
}
