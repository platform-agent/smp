var menuList =
    [
        {
            "id": 1,
            "name": "首页",
            "description": "描述信息",
            "url": contextPath + "/demo/to-forms-validation-page",
            "default": true,
            "pid": 0,
            "icon": "fa-home",
        },

        {
            "id": 2,
            "name": "Demo",
            "icon": "linecons-paper-plane",
            "default": false,
            "pid": 0,
            "lists": [
                {
                    "name": "表单验证",
                    "description": "基础的表单验证示例",
                    "url": contextPath + "/demo/to-forms-validation-page",
                    "pid": 2,
                    "icon": "linecons-note"
                },
                {
                    "name": "动态表格加载",
                    "description": "描述信息2",
                    "url": contextPath + "/demo/to-bootstrap-table-page",
                    "pid": 2,
                    "icon": "linecons-database"
                },
                {
                    "name": "404 Page",
                    "description": "页面没有找到提示页面",
                    "url": contextPath + "/portal/to-404-page",
                    "pid": 2,
                    "icon": "linecons-database"
                }
            ]
        },
        {
            "id": 3,
            "name": "系统管理",
            "icon": "linecons-cog",
            "default": false,
            "pid": 0,
            "lists": [
                {
                    "id": 3,
                    "name": "用户组织",
                    "icon": "linecons-user",
                    "default": false,
                    "pid": 0,
                    "lists": [
                        {
                            "name": "用户管理",
                            "description": "管理系统的用户列表, 可对用户进行基本的授权以及维护操作",
                            "url": contextPath + "/user/to-list-page",
                            "pid": 3
                        },
                        {
                            "name": "新增用户",
                            "description": "新增系统用户信息, 温馨提示: 新增用户初始化密码为: 666666",
                            "url": contextPath + "/user/to-edit-page",
                            "pid": 3
                        },
                        {
                            "name": "部门管理",
                            "description": "管理系统部门信息, 可进行部门新增，修改等操作",
                            "url": contextPath + "/group/to-list-page",
                            "pid": 3
                        }
                    ]
                } ,

                {
                    "id": 3,
                    "name": "菜单管理",
                    "icon": "linecons-params",
                    "default": false,
                    "pid": 0,
                    "lists": [
                        {
                            "name": "菜单管理",
                            "description": "管理系统的菜单栏目, 可对菜单进行基本的维护操作",
                            "url": contextPath + "/menu/to-list-page",
                            "pid": 3
                        }
                    ]
                },

                {
                    "id": 3,
                    "name": "权限管理",
                    "icon": "linecons-lock",
                    "default": false,
                    "pid": 0,
                    "lists": [
                        {
                            "name": "角色管理",
                            "description": "管理系统的角色列表,可配置用户角色信息以及授权菜单访问",
                            "url": contextPath + "/role/to-list-page",
                            "pid": 3
                        },
                        {
                            "name": "用户角色",
                            "description": "新增系统用户信息, 温馨提示: 新增用户初始化密码为: 666666",
                            "url": contextPath + "/user/to-edit-page",
                            "pid": 3
                        },
                        {
                            "name": "角色权限",
                            "description": "新增系统用户信息, 温馨提示: 新增用户初始化密码为: 666666",
                            "url": contextPath + "/user/to-edit-page",
                            "pid": 3
                        }
                    ]
                },

                {
                    "id": 3,
                    "name": "系统安全",
                    "icon": "linecons-fire",
                    "default": false,
                    "pid": 0,
                    "lists": [
                        {
                            "name": "操作日志",
                            "icon": "linecons-database",
                            "description": "系统操作日志记录",
                            "url": contextPath + "/user/to-list-page",
                            "pid": 3
                        },
                        {
                            "name": "登录日志",
                            "icon": "linecons-user",
                            "description": "系统用户登录日志记录",
                            "url": contextPath + "/user/to-list-page",
                            "pid": 3
                        }
                    ]
                }


            ]
        },
        {
            "id": 3,
            "name": "消息管理",
            "icon": "linecons-mail",
            "default": false,
            "pid": 0,
            "lists": [
                {
                    "name": "我的消息",
                    "description": "发送系统通知",
                    "url": contextPath + "/user/to-list-page",
                    "pid": 3
                },
                {
                    "name": "消息列表",
                    "description": "系统消息列表",
                    "url": contextPath + "/user/to-list-page",
                    "pid": 3
                },
                {
                    "name": "发送消息",
                    "description": "新增系统用户信息, 温馨提示: 新增用户初始化密码为: 666666",
                    "url": contextPath + "/user/to-edit-page",
                    "pid": 3
                }
            ]
        }
    ];
$(function () {
    //加载系统菜单
    loadMenu();

    //点击链接, 跳转界面
    $(".skip-page").click(function () {
        var url = $(this).attr("url");
        if (url) {

            $(this).buttonLoadingTimeOut();

            //移除所有选中样式
            $(".main-menu li").removeClass("active");

            //为当前点击链接添加选中样式
            $(this).parent("li").addClass("active");

            //为一级菜单添加选中样式
            $(this).parents(".main-menu > li").addClass("active");

            setEnvInfo($(this));

            //$("#main-menu").removeClass("mobile-is-visible");

            $("#content").attr("src", url);
        }
    });

    $("#sidebar").click(function () {
        //sidebar_menu_item_collapse($('.expanded'), $('.expanded > ul'))
        $('.expanded > a').click();

        //setTimeout(function(){ $("ul").removeAttr("style"); }, 1000);

    });

    //注意：下面的代码是放在和iframe同一个页面中调用
    /*
     $("#content").load(function () {
     var mainheight = $(this).contents().find("body").height() + 30;
     $(this).height(mainheight);
     });
     */
});

//设置导航HTML
function setEnvInfo(obj) {

    var title = obj.attr("title");
    var description = obj.attr("description");
    var icon = obj.attr("icon");
    var navpath = obj.attr("navpath");
    var navpaths = navpath.split(",");

    //遍历菜单上级名称
    var envHtml = '';
    $.each(navpaths, function (n, value) {
        envHtml += '<li>';
        if (icon && n == 0) {
            envHtml += '<i class="' + icon + '"></i>';
        }
        if (n == navpaths.length - 1) {
            envHtml += '<strong>';
            envHtml += value;
            envHtml += '</strong>';
        } else {
            envHtml += value;
        }
        envHtml += '</li>';
    });

    $(".title-env .title").html(title);
    $(".title-env .description").html(description);

    $(".breadcrumb-env > ol").empty();
    $(".breadcrumb-env > ol").append(envHtml);
}

//加载系统菜单
function loadMenu() {
    var $sidebar_nav = "";

    $.each(menuList, function (n, value) {
        var navpath = value.name;
        var icon = value.icon;
        $sidebar_nav += loadNextMenu(value, icon, navpath);
    });

    $("#main-menu").append($sidebar_nav);
}

//递归下级菜单
function loadNextMenu(value, picon, navpath) {

    var $sidebar_nav = "";

    var name = value.name;
    var defalut = value.default;
    var url = value.url;
    var icon = value.icon;
    var pid = value.pid;
    var lists = value.lists;
    var description = value.description ? value.description : "";

    //是否存在下级菜单
    if (lists) {
        $sidebar_nav += '<li>';
        $sidebar_nav += '<a href="#">';
        $sidebar_nav += '<i class="' + icon + '"></i>';
        $sidebar_nav += '<span class="title">' + name + '</span>';
        $sidebar_nav += '</a>';

        $sidebar_nav += '<ul>';
        var $sub_sidebar_nav = "";
        $.each(lists, function (n, subvalue) {
            //递归下机菜单
            $sub_sidebar_nav += loadNextMenu(subvalue, picon, navpath + ',' + subvalue.name);
        });

        $sidebar_nav += $sub_sidebar_nav;
        $sidebar_nav += '</ul>';

    } else {
        $sidebar_nav += '<li>';

        $sidebar_nav += '<a href="javascript:void(0);" class="skip-page" url="' + url + '" title="' + name + '" description="' + description + '" navpath="' + navpath + '" icon="' + picon + '">';
        $sidebar_nav += '<i class="' + icon + '"></i>';
        $sidebar_nav += '<span class="title">' + name + '</span>';
        $sidebar_nav += '</a>';

        $sidebar_nav += '</li>';

    }
    $sidebar_nav += '</li>';

    return $sidebar_nav;
}
