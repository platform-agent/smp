//用户列表JS
var $table = $('#user-list-table');
$(function () {
    //1.初始化Table
    var url = contextPath + '/user/list';//请求后台的URL（*）
    $table.SimpleTableInit("#search-form", url, Init().initColumn());

    $table.bootstrapTable("")

    //2.初始化Button的点击事件
    Init().InitButton();
});


//初始化信息
var Init = function () {
    var oInit = new Object();

    /**
     * 初始化列信息
     */
    oInit.initColumn = function () {
        var columns = [
            {checkbox: true},
            {
                field: 'account',
                title: '帐号',
                width: 120
            },
            {
                field: 'name',
                title: '昵称',
                width: 120
            },
            {
                field: 'mobile',
                title: '手机号码',
                width: 100
            },
            {
                field: 'email',
                title: '电子邮箱',
                width: 120
            }, {
                field: 'status',
                title: '状态',
                align: 'center',
                width: 50,
                formatter: formatter.status
            }, {
                field: 'createTime',
                title: '注册时间',
                align: 'center',
                width: 100,
                formatter: formatter.date2
            }, {
                field: 'mark',
                title: '备注',
                width: 180
            }, {
                title: '操作',
                width: 130,
                formatter: function (value, row, index) {
                    var edit = '<a href="#" class="btn btn-white btn-xs" onclick="Init().editInfo(\'' + row.uuid + '\')"><i class="linecons-pencil"></i>编辑</a>';

                    var resetPassw = '<a href="#" class="btn btn-white btn-xs" onclick="Init().resetPassw(\'' + row.uuid + '\')"><i class="linecons-key"></i>重置</a>';

                    return edit + resetPassw;
                }
            }
        ];
        return columns;
    }

    /**
     * 初始化页面按钮
     * @constructor
     */
    oInit.InitButton = function () {
        //修改用户信息模式窗口提交按钮绑定
        var url = contextPath + "/user/save";
        $(".btn-success").bindClickEvent4SubmitFrom("#user-edit-form", url, "POST", function () {
            $table.bootstrapTable('refresh');//刷新Table
            $('#user-info-modal').modal('hide');//隐藏模式窗口
        });

        //搜索按钮
        $("#search-button").click(function () {
            $(this).buttonLoadingTimeOut();
            $table.bootstrapTable('refresh');
        });

        //用户新增
        $("#user-new").click(function () {
            window.location.href = contextPath + "/user/to-edit-page"
        });

        //用户编辑
        $("#user-edit").click(function () {
            $(this).buttonLoadingTimeOut();
            var arrselections = $table.bootstrapTable('getSelections');
            if (arrselections.length > 1) {
                MessageTool.warning('只能选择一行数据进行编辑');
                return;
            }
            if (arrselections.length <= 0) {
                MessageTool.warning('请选择有效数据');
                return;
            }

            var uuid = arrselections[0].uuid
            oInit.editInfo(uuid);
        });

        //锁定账户
        $("#user-lock").click(function () {
            $(this).buttonLoadingTimeOut();
            var arrselections = $table.bootstrapTable('getSelections');
            if (arrselections.length <= 0) {
                MessageTool.warning('请选择有效数据');
                return;
            }

            var data = [];
            $.each(arrselections, function (index, value) {
                var uuid = value.uuid;
                data.push({uuid: uuid});
            });

            //弹出确认框
            var url = contextPath + "/user/lock";
            AjaxPlugin.Confirm('账户锁定', '确定锁定账户? 用户锁定状态后将无法登录系统!', '锁定', url, data, function () {
                $table.bootstrapTable('refresh');//刷新Table
            });

        });

        //解锁账户
        $("#user-unlock").click(function () {
            $(this).buttonLoadingTimeOut();
            var arrselections = $table.bootstrapTable('getSelections');
            if (arrselections.length <= 0) {
                MessageTool.warning('请选择有效数据');
                return;
            }

            var data = [];
            $.each(arrselections, function (index, value) {
                var uuid = value.uuid;
                data.push({uuid: uuid});
            });


            //弹出确认框
            var url = contextPath + "/user/unlock";
            AjaxPlugin.Confirm('账户解锁', '确定将账户解锁', '解锁', url, data, function () {
                $table.bootstrapTable('refresh');//刷新Table
            });

        });
    };

    /**
     * 显示编辑模式窗口
     * @param uuid
     */
    oInit.editInfo = function (uuid) {
        var url = contextPath + "/user/find-by-uuid";
        var data = {uuid: uuid}
        $("#user-edit-form").formSetData(url, data, 'POST', function (result) {
            $('#user-info-modal').modal('show');
            $("#s2example-2").val(null).trigger("change");
        });
    };

    /**
     * 重置密码
     * @param uuid
     */
    oInit.resetPassw = function(uuid) {
        var data = {uuid: uuid};
        var url = contextPath + "/user/reset-password";
        AjaxPlugin.Confirm('密码重置','确定重置用户密码? 密码重置后为: <small class="text" style="padding-top:5px;"><code>666666</code></small>','重置',url,data);
    };

    return oInit;
};