//组织部门JS
var $table = $('#list-table');//table
var $tree = $('#tree');//tree

var loadTreeUrl =  contextPath + '/group/load-tree-data';
var loadTableUrl = contextPath + '/group/list';//请求后台的URL（*）

var treeData;
$(function () {
    $("#puuid").val(0);//默认加载顶级List

    var init = new Init();
    init.InitTreeview();
    init.loadTable();
    init.InitButton();
});


//初始化页面按钮
var Init = function () {
    var oInit = new Object();

    /**
     * 初始化表格列信息
     * @returns {[*,*,*,*,*,*]}
     */
    oInit.initColumn = function(){
        var columns = [
            {
                checkbox: true
            },
            {
                field: 'code',
                title: '部门编码',
                width: 120
            },
            {
                field: 'name',
                title: '部门名称',
                width: 120
            },
            {
                field: 'status',
                title: '状态',
                align: 'center',
                width: 50,
                formatter: formatter.status
            }, {
                field: 'createTime',
                title: '创建时间',
                align: 'center',
                width: 100,
                formatter: formatter.date2
            }, {
                field: 'description',
                title: '描述',
                width: 180
            }
        ];
        return columns;
    }

    /**
     * 加载部门树
     */
    oInit.InitTreeview = function(){
        //加载树
        $tree.loadTreeview(loadTreeUrl , {} , function(data){
            $("#form-puuid").val(data.id);
            $table.tableRefresh();
            $tree.treeview('clearSearch');
        });
        //部门树结构 - 绑定搜索框
        $('#tree-search').bindTreeSearch($tree)
    };

    /**
     * 加载table
     */
    oInit.loadTable = function(){
        //1.初始化Table

        $table.SimpleTableInit("#search-form", loadTableUrl, oInit.initColumn());
    };

    /**
     * 初始化页面按钮
     * @constructor
     */
    oInit.InitButton = function () {
        //模式窗口 - 上级部门元素绑定显示树控件
        $("#parentName").bindClickShowTreeview(loadTreeUrl , {} , "#modal-puuid");

        //修改用户信息模式窗口提交按钮绑定
        var url = contextPath + "/group/save";
        $(".btn-success").bindClickEvent4SubmitFrom("#group-edit-form", url, "POST", function () {
            $table.bootstrapTable('refresh');//刷新Table
            $('#group-info-modal').modal('hide');//隐藏模式窗口
            oInit.InitTreeview();//重新加载窗口
        });

        //搜索按钮
        $("#search-button").click(function () {
            $(this).buttonLoadingTimeOut();
            $table.bootstrapTable('refresh');
        });

        //用户新增
        $("#user-new").click(function () {
            $("#group-edit-form").resetForm();//先清空表单
            var treeSelect = $tree.treeview('getSelected');
            $("#parentName").val(treeSelect[0].text);
            $("#modal-puuid").val(treeSelect[0].id);
            $('#group-info-modal').modal('show');
        });

        //用户编辑
        $("#user-edit").click(function () {
            $(this).buttonLoadingTimeOut();
            var arrselections = $table.bootstrapTable('getSelections');
            if (arrselections.length > 1) {
                MessageTool.warning('只能选择一行数据进行编辑');
                return;
            }
            if (arrselections.length <= 0) {
                MessageTool.warning('请选择有效数据');
                return;
            }

            var uuid = arrselections[0].uuid
            oInit.editInfo(uuid);
        });

        //锁定账户
        $("#user-lock").click(function () {
            $(this).buttonLoadingTimeOut();
            var arrselections = $table.bootstrapTable('getSelections');
            if (arrselections.length <= 0) {
                MessageTool.warning('请选择有效数据');
                return;
            }

            var data = [];
            $.each(arrselections, function (index, value) {
                var uuid = value.uuid;
                data.push({uuid: uuid});
            });

            //弹出确认框
            var url = contextPath + "/group/lock";
            AjaxPlugin.Confirm('部门锁定', '确定锁定部门?', '锁定', url, data, function () {
                $table.bootstrapTable('refresh');//刷新Table
                oInit.InitTreeview();//重新加载窗口
            });

        });

        //解锁账户
        $("#user-unlock").click(function () {
            $(this).buttonLoadingTimeOut();
            var arrselections = $table.bootstrapTable('getSelections');
            if (arrselections.length <= 0) {
                MessageTool.warning('请选择有效数据');
                return;
            }

            var data = [];
            $.each(arrselections, function (index, value) {
                var uuid = value.uuid;
                data.push({uuid: uuid});
            });


            //弹出确认框
            var url = contextPath + "/group/unlock";
            AjaxPlugin.Confirm('部门解锁', '确定将部门解锁', '解锁', url, data, function () {
                $table.bootstrapTable('refresh');//刷新Table
                oInit.InitTreeview();//重新加载窗口
            });

        });
    };

    /**
     * 弹出模式窗口编辑信息
     * @param uuid
     */
    oInit.editInfo = function (uuid) {
        var url = contextPath + "/group/find-by-uuid";
        var data = {uuid: uuid}
        $("#group-edit-form").formSetData(url, data, 'POST', function () {
            $('#group-info-modal').modal('show');
            $("#s2example-2").val(null).trigger("change");
        });
    };

    return oInit;
};

