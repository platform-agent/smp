//用户列表JS

var $table = $('#list-table');
var url = contextPath + '/role/list';//请求后台的URL（*）
$(function () {
    var oInit = new Init();

    //1.初始化Table
    $table.SimpleTableInit("#search-form", url, oInit.InitColumn());

    //2.初始化Button的点击事件
    oInit.InitButton();
});

//初始化页面按钮
var Init = function () {
    var oInit = new Object();
    /**
     * 初始化列
     * @returns {[*,*,*,*,*,*,*]}
     * @constructor
     */
    oInit.InitColumn = function(){
        var columns = [
            {
                checkbox: true
            },
            {
                field: 'code',
                title: '角色编码',
                width: 120
            },
            {
                field: 'name',
                title: '角色名称',
                width: 120
            },
            {
                field: 'status',
                title: '状态',
                align: 'center',
                width: 50,
                formatter: formatter.status
            }, {
                field: 'createTime',
                title: '创建时间',
                align: 'center',
                width: 100,
                formatter: formatter.date2
            }, {
                field: 'description',
                title: '描述',
                width: 180
            }, {
                title: '操作',
                width: 130,
                formatter: function (value, row, index) {
                    var edit = '<a href="#" class="btn btn-white btn-xs" onclick="ButtonInit().editInfo(\'' + row.uuid + '\')"><i class="linecons-pencil"></i>编辑</a>';
                    return edit;
                }
            }
        ];

        return columns;

    }

    oInit.InitButton = function () {
        //修改用户信息模式窗口提交按钮绑定
        var url = contextPath + "/role/save";
        $(".btn-success").bindClickEvent4SubmitFrom("#edit-form", url, "POST", function () {
            $table.bootstrapTable('refresh');//刷新Table
            $('#info-modal').modal('hide');//隐藏模式窗口
        });

        //搜索按钮
        $("#search-button").click(function () {
            $(this).buttonLoadingTimeOut();
            $table.bootstrapTable('refresh');
        });

        //新增
        $("#new").click(function () {
            $("#edit-form").resetForm();//先清空表单
            $('#info-modal').modal('show');
        });

        //编辑
        $("#edit").click(function () {
            $(this).buttonLoadingTimeOut();
            var arrselections = $table.bootstrapTable('getSelections');
            if (arrselections.length > 1) {
                MessageTool.warning('只能选择一行数据进行编辑');
                return;
            }
            if (arrselections.length <= 0) {
                MessageTool.warning('请选择有效数据');
                return;
            }

            var uuid = arrselections[0].uuid
            oInit.editInfo(uuid);
        });

        //锁定
        $("#lock").click(function () {
            $(this).buttonLoadingTimeOut();
            var arrselections = $table.bootstrapTable('getSelections');
            if (arrselections.length <= 0) {
                MessageTool.warning('请选择有效数据');
                return;
            }

            var data = [];
            $.each(arrselections, function (index, value) {
                var uuid = value.uuid;
                data.push({uuid: uuid});
            });

            //弹出确认框
            var url = contextPath + "/role/lock";
            AjaxPlugin.Confirm('锁定', '确定锁定?', '锁定', url, data, function () {
                $table.bootstrapTable('refresh');//刷新Table
            });

        });

        //解锁
        $("#unlock").click(function () {
            $(this).buttonLoadingTimeOut();
            var arrselections = $table.bootstrapTable('getSelections');
            if (arrselections.length <= 0) {
                MessageTool.warning('请选择有效数据');
                return;
            }

            var data = [];
            $.each(arrselections, function (index, value) {
                var uuid = value.uuid;
                data.push({uuid: uuid});
            });


            //弹出确认框
            var url = contextPath + "/role/unlock";
            AjaxPlugin.Confirm('解锁', '确定解锁', '解锁', url, data, function () {
                $table.bootstrapTable('refresh');//刷新Table
            });

        });
    };

    oInit.editInfo = function (uuid) {
        var url = contextPath + "/role/find-by-uuid";
        var data = {uuid: uuid}
        $("#edit-form").formSetData(url, data, 'POST', function (result) {
            $('#info-modal').modal('show');
            $("#s2example-2").val(null).trigger("change");
        });
    };
    return oInit;
};