/**
 * Created by SIVEN on 17/4/11.
 */

__CreateJSPath = function (js) {
    var scripts = document.getElementsByTagName("script");
    var path = "";
    for (var i = 0, l = scripts.length; i < l; i++) {
        var src = scripts[i].src;
        if (src.indexOf(js) != -1) {
            var ss = src.split(js);
            path = ss[0];
            break;
        }
    }
    var href = location.href;
    href = href.split("#")[0];
    href = href.split("?")[0];
    var ss = href.split("/");
    ss.length = ss.length - 1;
    href = ss.join("/");
    if (path.indexOf("https:") == -1 && path.indexOf("http:") == -1 && path.indexOf("file:") == -1 && path.indexOf("\/") != 0) {
        path = href + "/" + path;
    }
    return path;
}

var bootPATH = __CreateJSPath("boot.js");

var contextPath = getContextPath();

function getContextPath() {
    var pathName = document.location.pathname;
    if (pathName.indexOf("/pages") == 0) {
        return "";
    }
    var index = pathName.substr(1).indexOf("/");
    var result = pathName.substr(0, index + 1);
    return result;
}

//引入CSS
document.write('<style type="text/css">');

document.write('@import url("' + bootPATH + 'css/fonts/linecons/css/linecons.css");');
document.write('@import url("' + bootPATH + 'css/fonts/fontawesome/css/font-awesome.min.css");');
document.write('@import url("' + bootPATH + 'css/bootstrap.css");');
document.write('@import url("' + bootPATH + 'css/xenon-core.css");');
document.write('@import url("' + bootPATH + 'css/xenon-forms.css");');
document.write('@import url("' + bootPATH + 'css/xenon-components.css");');
document.write('@import url("' + bootPATH + 'css/xenon-skins.css");');
document.write('@import url("' + bootPATH + 'css/custom.css");');
document.write('@import url("' + bootPATH + 'js/datatables/bootstrap-table.min.css");');
document.write('@import url("' + bootPATH + 'js/treeview/bootstrap-treeview.min.css");');
document.write('@import url("' + bootPATH + 'js/daterangepicker/daterangepicker-bs3.css");');
document.write('@import url("' + bootPATH + 'js/select2/select2.css");');
document.write('@import url("' + bootPATH + 'js/select2/select2-bootstrap.css");');
document.write('@import url("' + bootPATH + 'js/multiselect/css/multi-select.css");');

document.write('</style>');


//JS
document.write('<script src="' + bootPATH + 'js/jquery-1.11.1.min.js" type="text/javascript" ></sc' + 'ript>');

document.write('<script src="' + bootPATH + 'js/bootstrap.min.js" type="text/javascript" ></sc' + 'ript>');

document.write('<script src="' + bootPATH + 'js/treeview/bootstrap-treeview.js" type="text/javascript" ></sc' + 'ript>');
document.write('<script src="' + bootPATH + 'js/datatables/bootstrap-table.min.js" type="text/javascript" ></sc' + 'ript>');
// document.write('<script src="' + bootPATH + 'js/datatables/bootstrap-table.js" type="text/javascript" ></sc' + 'ript>');
document.write('<script src="' + bootPATH + 'js/datatables/extensions/export/bootstrap-table-export.min.js" type="text/javascript" ></sc' + 'ript>');
document.write('<script src="' + bootPATH + 'js/datatables/extensions/export/tableExport.js" type="text/javascript" ></sc' + 'ript>');
document.write('<script src="' + bootPATH + 'js/datatables/locale/bootstrap-table-zh-CN.min.js" type="text/javascript" ></sc' + 'ript>');

document.write('<script src="' + bootPATH + 'js/datepicker/bootstrap-datepicker.js" type="text/javascript" ></sc' + 'ript>');
document.write('<script src="' + bootPATH + 'js/daterangepicker/daterangepicker.js" type="text/javascript" ></sc' + 'ript>');
document.write('<script src="' + bootPATH + 'js/timepicker/bootstrap-timepicker.min.js" type="text/javascript" ></sc' + 'ript>');
document.write('<script src="' + bootPATH + 'js/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript" ></sc' + 'ript>');
document.write('<script src="' + bootPATH + 'js/tagsinput/bootstrap-tagsinput.min.js" type="text/javascript" ></sc' + 'ript>');

document.write('<script src="' + bootPATH + 'js/TweenMax.min.js" type="text/javascript" ></sc' + 'ript>');
document.write('<script src="' + bootPATH + 'js/resizeable.js" type="text/javascript" ></sc' + 'ript>');
document.write('<script src="' + bootPATH + 'js/joinable.js" type="text/javascript" ></sc' + 'ript>');
document.write('<script src="' + bootPATH + 'js/xenon-api.js" type="text/javascript" ></sc' + 'ript>');
document.write('<script src="' + bootPATH + 'js/xenon-toggles.js" type="text/javascript" ></sc' + 'ript>');
document.write('<script src="' + bootPATH + 'js/toastr/toastr.min.js" type="text/javascript" ></sc' + 'ript>');

document.write('<script src="' + bootPATH + 'js/jquery-validate/jquery.validate.min.js" type="text/javascript" ></sc' + 'ript>');
document.write('<script src="' + bootPATH + 'js/jquery-validate/localization/messages_zh.min.js" type="text/javascript" ></sc' + 'ript>');
document.write('<script src="' + bootPATH + 'js/jquery-formautofill/jquery.formautofill.js" type="text/javascript" ></sc' + 'ript>');
document.write('<script src="' + bootPATH + 'js/jquery-form/jquery.form.js" type="text/javascript" ></sc' + 'ript>');
document.write('<script src="' + bootPATH + 'js/jquery-ui/jquery-ui.min.js" type="text/javascript" ></sc' + 'ript>');
document.write('<script src="' + bootPATH + 'js/inputmask/jquery.inputmask.bundle.js" type="text/javascript" ></sc' + 'ript>');

document.write('<script src="' + bootPATH + 'js/moment.min.js" type="text/javascript" ></sc' + 'ript>');
document.write('<script src="' + bootPATH + 'js/select2/select2.min.js" type="text/javascript" ></sc' + 'ript>');
document.write('<script src="' + bootPATH + 'js/selectboxit/jquery.selectBoxIt.min.js" type="text/javascript" ></sc' + 'ript>');
document.write('<script src="' + bootPATH + 'js/multiselect/js/jquery.multi-select.js" type="text/javascript" ></sc' + 'ript>');
document.write('<script src="' + bootPATH + 'js/typeahead.bundle.js" type="text/javascript" ></sc' + 'ript>');
document.write('<script src="' + bootPATH + 'js/handlebars.min.js" type="text/javascript" ></sc' + 'ript>');

//主题JS
document.write('<script src="' + bootPATH + 'js/xenon-custom.js" type="text/javascript" ></sc' + 'ript>');

//自定义通用JS加载
document.write('<script src="' + bootPATH + 'pagejs/common/common-tools.js" type="text/javascript" ></sc' + 'ript>');
document.write("<script src='" + bootPATH + "pagejs/common/ui-extension-tools.js' type='text/javascript'></script>");
document.write('<script src="' + bootPATH + 'pagejs/common/confirm.js" type="text/javascript" ></sc' + 'ript>');
document.write("<script src='" + bootPATH + "pagejs/common/ajax-plugin.js' type='text/javascript'></script>");
document.write("<script src='" + bootPATH + "pagejs/common/table-formatter.js' type='text/javascript'></script>");