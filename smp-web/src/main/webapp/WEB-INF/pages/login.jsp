<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>SMP - 用户登录 </title>

    <script src="${pageContext.request.contextPath}/assets/boot.js"></script>
    <script src="${pageContext.request.contextPath}/assets/pagejs/login.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="${pageContext.request.contextPath}/assets/js/html5shiv/html5shiv.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/respond/respond.min.js"></script>
    <![endif]-->
	
</head>
<body class="page-body login-page login-light">

	
	<div class="login-container">
	
		<div class="row">
		
			<div class="col-sm-6">
			
				<!-- Errors container -->
				<div class="errors-container">

				</div>
				
				<!-- Add class "fade-in-effect" for login form effect -->
				<form method="post" role="form" id="login" class="login-form fade-in-effect">
					
					<div class="login-header">
						<a href="dashboard-1.html" class="logo">
							<img src="${pageContext.request.contextPath}/assets/images/logo-white-bg@2x.png" alt="" width="80" />
							<span>登 录</span>
						</a>
						
						<p>亲爱的用户,登录进入管理系统!</p>
					</div>
	
					
					<div class="form-group">
						<label class="control-label" for="account">账户</label>
						<input type="text" class="form-control" name="account" id="account" autocomplete="off" />
					</div>
					
					<div class="form-group">
						<label class="control-label" for="password">密码</label>
						<input type="password" class="form-control" name="password" id="password" autocomplete="off"/>
					</div>
					
					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-block text-left" id="login-submit">
							<i class="fa-lock"></i>
							登 录
						</button>
					</div>
					
					<div class="login-footer">
						<a href="#">忘记密码?</a>
						
						<div class="info-links">
							<a href="#">ToS</a> -
							<a href="#">Privacy Policy</a>
						</div>
						
					</div>
					
				</form>

			</div>
			
		</div>
		
	</div>
</body>
<script>
    //window.history.forward(1);
    //防止页面后退
    history.pushState(null, null, document.URL);
    window.addEventListener('popstate', function () {
        history.pushState(null, null, document.URL);
    });
</script>
</html>