﻿<%--suppress JSUnresolvedFunction --%>
<%--suppress ALL --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SMP - 用户列表</title>

    <script src="${pageContext.request.contextPath}/assets/boot.js"></script>
    <script src="${pageContext.request.contextPath}/assets/pagejs/frame/content.js"></script>


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="${pageContext.request.contextPath}/assets/js/html5shiv/html5shiv.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/respond/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="page-container">


    <div class="panel panel-default" style="padding-top: 0px">

        <div class="panel-body">

            <!-- search -->
            <%--<div class="panel panel-flat panel-color panel-success collapsed">--%>
            <div class="panel panel-flat panel-color panel-info collapsed">
            <%--<div class="panel panel-flat panel-color panel-default collapsed">--%>
                <div class="panel-heading" style="padding: 10px 30px ; font-size: 15px">
                    <h3 class="panel-title">
                        <a href="#" data-toggle="panel">
                            <i class="linecons-search"></i>搜索
                        </a>
                    </h3>

                    <div class="panel-options">
                        <a href="#" data-toggle="panel">
                            <span class="collapse-icon">&ndash;</span>
                            <span class="expand-icon">+</span>
                        </a>
                        <a href="#" data-toggle="remove">
                            &times;
                        </a>
                    </div>
                </div>

                <div class="panel-body">
                    <form role="form" id="search-form" method="post">

                        <div class="row">

                            <div class="col-xs-4">

                                <div class="form-group">
                                    <label class="control-label">角色编码</label>

                                    <input type="text" class="form-control" name="code">
                                </div>

                            </div>

                            <div class="col-xs-4">

                                <div class="form-group">
                                    <label class="control-label">角色名称</label>
                                    <input type="text" class="form-control" name="name">
                                </div>

                            </div>

                            <div class="col-xs-4">

                                <div class="form-group">
                                    <label for="status" class="control-label">状态</label>
                                    <select class="form-control select2" id="search_status" name="status">
                                        <option value="" selected>请选择</option>
                                        <option value="Y">有效</option>
                                        <option value="N">无效</option>
                                    </select>
                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12" style="text-align: right;">
                                <button type="button" class="btn btn-info btn-primary" id="search-button">
                                    <i class="fa-search"></i>
                                    搜 索
                                </button>
                                <button type="button" class="btn btn-white" onclick="$('#search-form').resetForm();"> 重 置</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer"></div>
            <!-- search end ..-->

            <!-- Table 工具栏 -->
            <div style="width: 120px">
                <div id="toolbar" class="btn-group">
                    <a type="button" class="btn btn-white btn-sm linecons-user" id="new"> 新 增 </a>
                    <a type="button" class="btn btn-white btn-sm linecons-pencil" id="edit"> 编 辑 </a>
                    <a type="button" class="btn btn-white btn-sm linecons-lock" id="lock"> 锁 定 </a>
                    <a type="button" class="btn btn-white btn-sm linecons-eye" id="unlock"> 解 锁 </a>
                </div>

            </div>
            <table id="list-table"></table>
        </div>

    </div>

    <!-- Modal 编辑-->
    <div class="modal fade" id="info-modal" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">


                <div class="modal-header" style="border-bottom: none;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>


                <div class="modal-body">
                    <form role="form" id="edit-form" method="post" class="validate">

                        <input type="text" name="uuid" id="uuid" class="hidden-input"/>
                        <input type="text" name="createTime" id="createTime" class="hidden-input"/>

                        <div class="form-group">
                            <label class="control-label">角色编码</label>

                            <input type="text" class="form-control" name="code" data-validate="required" maxlength="20" placeholder="请输入角色编码"/>
                        </div>

                        <div class="form-group">
                            <label class="control-label">角色名称</label>

                            <input type="text" class="form-control" name="name" data-validate="required" placeholder="请输入角色名称" maxlength="30"/>
                        </div>

                        <div class="form-group">
                            <label class="control-label">有效状态</label>
                            <select class="form-control select2" id="status" name="status" required placeholder="请选择状态">
                                <option value="Y">有效</option>
                                <option value="N">无效</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label">描述</label>
                            <textarea class="form-control autogrow" placeholder="" name="description" rows="5" maxlength="100"></textarea>
                        </div>

                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-success">
                        <i class="fa-check"></i>
                        <span> 提 交 </span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal 用户编辑 end-->
</div>
<div class="page-loading-overlay">
    <div class="loader-2"></div>
</div>
<script src="${pageContext.request.contextPath}/assets/pagejs/role-manager/role-list.js"></script>
</body>
</html>