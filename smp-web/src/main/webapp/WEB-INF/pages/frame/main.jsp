<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content=""/>

    <title>SMP - 系统管理平台</title>

    <script src="${pageContext.request.contextPath}/assets/boot.js"></script>
    <!-- 加载页面菜单JS -->
    <script src="${pageContext.request.contextPath}/assets/pagejs/frame/menu.js"></script>
    <script src="${pageContext.request.contextPath}/assets/pagejs/frame/main.js"></script>


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="${pageContext.request.contextPath}/assets/js/html5shiv/html5shiv.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/respond/respond.min.js"></script>
    <![endif]-->
</head>

<style>
    .validate-has-error {
        color: red;
    }
</style>

<body class="page-body skin-navy">

<!-- settings-pane -->
<div class="settings-pane">

    <a href="#" data-toggle="settings-pane" data-animate="true">
        &times;
    </a>

    <div class="settings-pane-inner">

        <div class="row">

            <div class="col-md-4">

                <div class="user-info">

                    <div class="user-image">
                        <a href="extra-profile.html">
                            <img src="${pageContext.request.contextPath}/assets/images/user-2.png"
                                 class="img-responsive img-circle"/>
                        </a>
                    </div>

                    <div class="user-details">

                        <h3>
                            <a href="extra-profile.html" id="user-details-name"></a>

                            <!-- Available statuses: is-online, is-idle, is-busy and is-offline -->
                            <span class="user-status is-online"></span>
                        </h3>

                        <p class="user-title">Web Developer</p>

                        <div class="user-links">
                            <a href="extra-profile.html" class="btn btn-primary">Edit Profile</a>
                            <a href="extra-profile.html" class="btn btn-success">Upgrade</a>
                        </div>

                    </div>

                </div>

            </div>


        </div>

    </div>

</div>
<!-- end settings-pane -->

<div class="page-container">
    <!-- add class "sidebar-collahas-subpsed" to close sidebar by default, "chat-visible" to make chat appear always -->

    <!-- Add "fixed" class to make the sidebar fixed always to the browser viewport. -->
    <!-- Adding class "toggle-others" will keep only one menu item open at a time. -->
    <!-- Adding class "collapsed" collapse sidebar root elements and show only icons. -->
    <div class="sidebar-menu toggle-others fixed">

        <div class="sidebar-menu-inner">

            <header class="logo-env">

                <!-- logo -->
                <div class="logo">
                    <a href="dashboard-1.html" class="logo-expanded">
                        <img src="${pageContext.request.contextPath}/assets/images/logo@2x.png" width="80" alt=""/>
                    </a>

                    <a href="dashboard-1.html" class="logo-collapsed">
                        <img src="${pageContext.request.contextPath}/assets/images/logo-collapsed@2x.png" width="40"
                             alt=""/>
                    </a>
                </div>

                <!-- This will toggle the mobile menu and will be visible only on mobile devices -->
                <div class="mobile-menu-toggle visible-xs">
                    <a href="#" data-toggle="user-info-menu">
                        <i class="fa-bell-o"></i>
                        <span class="badge badge-success">7</span>
                    </a>

                    <a href="#" data-toggle="mobile-menu">
                        <i class="fa-bars"></i>
                    </a>
                </div>

                <!-- This will open the popup with user profile settings, you can use for any purpose, just be creative -->
                <div class="settings-icon">
                    <a href="#" data-toggle="settings-pane" data-animate="true">
                        <i class="linecons-cog"></i>
                    </a>
                </div>


            </header>


            <!-- 菜单栏目 -->
            <ul id="main-menu" class="main-menu" style="z-index: -1;">
                <!-- add class "multiple-expanded" to allow multiple submenus to open -->
                <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
            </ul>

        </div>

    </div>

    <div class="main-content">

        <!-- User Info, Notifications and Menu Bar -->
        <nav class="navbar user-info-navbar" role="navigation">

            <!-- Left links for user info navbar -->
            <ul class="user-info-menu left-links list-inline list-unstyled">

                <li class="hidden-sm hidden-xs">
                    <a href="#" data-toggle="sidebar" id="sidebar">
                        <i class="fa-bars"></i>
                    </a>
                </li>

                <li class="dropdown hover-line">
                    <a href="#" data-toggle="dropdown">
                        <i class="fa-envelope-o"></i>
                        <span class="badge badge-green">15</span>
                    </a>

                    <ul class="dropdown-menu messages">
                        <li>

                            <ul class="dropdown-menu-list list-unstyled ps-scrollbar">

                                <li class="active">
                                    <!-- "active" class means message is unread -->
                                    <a href="#">
                                                <span class="line">
												<strong>Luc Chartier</strong>
												<span class="light small">- yesterday</span>
                                                </span>

                                        <span class="line desc small">
												This ain’t our first item, it is the best of the rest.
											</span>
                                    </a>
                                </li>

                            </ul>

                        </li>

                        <li class="external">
                            <a href="blank-sidebar.html">
                                <span>所有消息</span>
                                <i class="fa-link-ext"></i>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="dropdown hover-line">
                    <a href="#" data-toggle="dropdown">
                        <i class="fa-bell-o"></i>
                        <span class="badge badge-purple">7</span>
                    </a>

                    <ul class="dropdown-menu notifications">
                        <li class="top">
                            <p class="small">
                                <a href="#" class="pull-right">标记为已读</a> 你有 <strong>3</strong> 新的通知信息
                            </p>
                        </li>

                        <li>
                            <ul class="dropdown-menu-list list-unstyled ps-scrollbar">

                                <li class="notification-primary">
                                    <a href="#">
                                        <i class="fa-thumbs-up"></i>

                                        <span class="line">
												<strong>Someone special liked this</strong>
											</span>

                                        <span class="line small time">
												2 minutes ago
											</span>
                                    </a>
                                </li>

                            </ul>
                        </li>

                        <li class="external">
                            <a href="#">
                                <span>查看所有通知</span>
                                <i class="fa-link-ext"></i>
                            </a>
                        </li>
                    </ul>
                </li>

            </ul>


            <!-- Right links for user info navbar -->
            <ul class="user-info-menu right-links list-inline list-unstyled">

                <li class="search-form">
                    <!-- You can add "always-visible" to show make the search input visible -->

                    <form method="get" action="extra-search.html">
                        <input type="text" name="s" class="form-control search-field" placeholder="Type to search..."/>

                        <button type="submit" class="btn btn-link">
                            <i class="linecons-search"></i>
                        </button>
                    </form>

                </li>

                <!-- 用户信息 -->
                <li class="dropdown user-profile">
                    <a href="#" data-toggle="dropdown">
                        <img src="${pageContext.request.contextPath}/assets/images/user-4.png" alt="user-image"
                             class="img-circle img-inline userpic-32" width="28"/>
                        <span>

								<i class="fa-angle-down"></i>
							</span>
                    </a>

                    <ul class="dropdown-menu user-profile-menu list-unstyled">
                        <li>
                            <a href="#" id="chang-password">
                                <i class="fa-key"></i> 修改密码
                            </a>
                        </li>

                        <li>
                            <a href="#help">
                                <i class="fa-info"></i> 帮助
                            </a>
                        </li>
                        <li class="last">
                            <a href="${pageContext.request.contextPath}/user-login/logout">
                                <i class="fa-lock"></i> 退出登录
                            </a>
                        </li>
                    </ul>
                </li>

            </ul>

        </nav>

        <!-- User Info, Notifications and Menu Bar -->
        <div class="page-title">

            <div class="title-env">
                <h1 class="title">我的工作台</h1>
                <p class="description"></p>
            </div>

            <div class="breadcrumb-env">

                <ol class="breadcrumb bc-1">
                    <li>
                        <i class="fa-home"></i>首页
                    </li>
                </ol>

            </div>
        </div>

        <!-- 详细页面元素 -->
        <iframe width="100%" frameborder="0" height="100" scrolling="no" id="content"
                src="${pageContext.request.contextPath}/pages/demo/forms-validation.jsp"></iframe>


        <!-- Main Footer -->
        <!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
        <!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
        <!-- Or class "fixed" to  always fix the footer to the end of page -->
        <footer class="main-footer">

            <div class="footer-inner">

                <!-- Add your copyright text here -->
                <div class="footer-text">
                    &copy; 2017
                    <strong>SMP</strong> 系统管理平台 from SIVEN
                </div>


                <!-- Go to Top Link, just add rel="go-top" to any link to add this functionality -->
                <div class="go-up">

                    <a href="#" rel="go-top">
                        <i class="fa-angle-up"></i>
                    </a>

                </div>

            </div>

        </footer>
    </div>

</div>


<!-- 模式窗口 Modal 修改密码-->
<div class="modal fade" id="change-password-modal" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <i class="linecons-key"></i>修改密码
            </div>


            <form method="post" id="change-password-form">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="oldPassword" class="control-label">旧的密码</label>
                                <input type="password" class="form-control" id="oldPassword" name="oldPassword"
                                       maxlength="20"/>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="newPassword" class="control-label">新的密码</label>
                                <input type="password" class="form-control" id="newPassword" name="newPassword"
                                       maxlength="20"/>
                            </div>

                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="confirmPassword" class="control-label">确认密码</label>
                                <input type="password" class="form-control" id="confirmPassword" name="confirmPassword"
                                       maxlength="20"/>
                            </div>

                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
                    <button type="submit" class="btn btn-success" id="change-password-button" autocomplete="off">
                        <i class="fa-check"></i>
                        <span> 提 交 </span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- 模式窗口 Modal 修改密码 end-->


<!-- 模式窗口 Modal-->
<div class="modal fade" id="main-modal" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            -->
            <div class="modal-body" id="main-modal-body">

            </div>
        </div>
    </div>
</div>

<div class="page-loading-overlay">
    <div class="loader-2"></div>
</div>
<script src="${pageContext.request.contextPath}/assets/js/xenon-custom.js" type="text/javascript"></script>
</body>

<script>
    //提示用户修改密码
    var isInitializePass = ${isInitializePass};
    if (isInitializePass) {
        MessageTool.warning("您的密码为初始密码, 请修改!");
        $("#change-password-form").resetForm();
        $("#change-password-modal").modal('show');
    }
    window.history.forward(1);
</script>
</html>