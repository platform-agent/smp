<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>404 Page Not Find..</title>

    <script src="${pageContext.request.contextPath}/assets/boot.js"></script>
    <script src="${pageContext.request.contextPath}/assets/pagejs/frame/content.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="${pageContext.request.contextPath}/assets/js/html5shiv/html5shiv.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/respond/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<div class="page-container">
    <div class="page-error centered">
        <div class="error-symbol">
            <i class="fa-warning"></i>
        </div>

        <h2>
            Error 404
            <small>页面没有找到!</small>
        </h2>

        <p>我们没有找到您尝试打开的页面!</p>
        <p>你可以搜索或联系我们的管理员来帮助你!</p>

    </div>

    <div class="page-error-search centered">
        <form class="form-half" method="get" action="" enctype="application/x-www-form-urlencoded">
            <input type="text" class="form-control input-lg" placeholder="Search..."/>

            <button type="submit" class="btn-unstyled">
                <i class="linecons-search"></i>
            </button>
        </form>

        <a href="#" class="go-back">
            <i class="fa-angle-left"></i>
            返回
        </a>
    </div>
</div>


</body>
</html>