﻿<%--suppress JSUnresolvedFunction --%>
<%--suppress ALL --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SMP - 用户列表</title>

    <script src="${pageContext.request.contextPath}/assets/boot.js"></script>
    <script src="${pageContext.request.contextPath}/assets/pagejs/frame/content.js"></script>


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="${pageContext.request.contextPath}/assets/js/html5shiv/html5shiv.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/respond/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="page-container">

    <div class="row">

        <!-- Gallery Sidebar -->
        <div class="col-sm-3" style="padding-right: 0px;margin: 0px;">
            <div class="form-group">
                <input type="text" class="form-control" id="tree-search" placeholder="搜索">
            </div>
            <div id="tree" class="treeview"></div>
            <form id="search-form">
                <input type="hidden" name="puuid" id="form-puuid" value="0"/>
            </form>
        </div>

        <!-- Gallery Album Optipns and Images -->
        <div class="col-sm-9" style="padding-left: 5px">
            <div class="panel panel-default" id="group-list-panel">
                <div class="panel-body">
                    <!-- Table 工具栏 -->
                    <div style="width: 120px">
                        <div id="toolbar" class="btn-group">
                            <a type="button" class="btn btn-white btn-sm linecons-user" id="user-new"> 新 增 </a>
                            <a type="button" class="btn btn-white btn-sm linecons-pencil" id="user-edit"> 编 辑 </a>
                            <a type="button" class="btn btn-white btn-sm linecons-lock" id="user-lock"> 锁 定 </a>
                            <a type="button" class="btn btn-white btn-sm linecons-eye" id="user-unlock"> 解 锁 </a>
                        </div>

                    </div>
                    <table id="list-table"></table>
                </div>
            </div>

        </div>

    </div>
</div>


<!-- Modal 部门编辑-->
<div class="modal fade" id="group-info-modal" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: none;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>

            <div class="modal-body">
                <form role="form" id="group-edit-form" method="post" class="validate">

                    <input type="text" name="uuid" id="uuid" class="hidden-input"/>
                    <input type="text" name="createTime" id="createTime" class="hidden-input"/>

                    <div class="form-group">
                        <label class="control-label">部门编码</label>
                        <input type="text" class="form-control" name="code" data-validate="required" maxlength="20" data-mask="string"/>
                    </div>

                    <div class="form-group">
                        <label class="control-label">部门名称</label>

                        <input type="text" class="form-control" name="name" data-validate="required"
                               placeholder="请输入部门名称" maxlength="20"/>
                    </div>

                    <div class="form-group">
                        <label class="control-label">上级部门</label>
                        <input type="text" id="parentName" name="parentName" class="form-control" data-validate="required" placeholder="请选择上级部门" readonly style="cursor:pointer" />
                        <input type="text" id="modal-puuid" name="puuid" class="hidden-input"/>
                    </div>

                    <div class="form-group">
                        <label class="control-label">有效状态</label>
                        <script type="text/javascript">
                            jQuery(document).ready(function ($) {
                                $("#status").select2({
                                    allowClear: true
                                }).on('select2-open', function () {
                                    // Adding Custom Scrollbar
                                    $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
                                });
                            });
                        </script>
                        <select class="form-control" id="status" name="status" required placeholder="请选择状态">
                            <option value="Y">有效</option>
                            <option value="N">无效</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="control-label">描述</label>
                        <textarea class="form-control autogrow" placeholder="" name="description" rows="5"
                                  maxlength="100"></textarea>
                    </div>

                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-success">
                    <i class="fa-check"></i>
                    <span> 提 交 </span>
                </button>
            </div>
        </div>
    </div>
</div>
<!-- Modal 部门编辑 end-->

<div class="page-loading-overlay">
    <div class="loader-2"></div>
</div>
<script src="${pageContext.request.contextPath}/assets/pagejs/group-manager/group-list.js"></script>
</body>
</html>