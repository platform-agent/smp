<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SMP - 用户锁定 </title>

    <script src="${pageContext.request.contextPath}/assets/boot.js"></script>
    <script src="${pageContext.request.contextPath}/assets/pagejs/lockscreen.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="${pageContext.request.contextPath}/assets/js/html5shiv/html5shiv.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/respond/respond.min.js"></script>
    <![endif]-->

</head>
<body class="page-body lockscreen-page">

<div class="login-container">

    <div class="row">

        <div class="col-sm-7">
            <form role="form" id="lockscreen" class="lockcreen-form fade-in-effect">

                <div class="user-thumb">
                    <a href="#">
                        <img src="${pageContext.request.contextPath}/assets/images/user-5.png"
                             class="img-responsive img-circle"/>
                    </a>
                </div>

                <div class="form-group">
                    <h3>欢迎回来,${userinfo.userNickName}! </h3>
                    <p>输入您的密码来访问系统。或者使用
                        <a style="color: #d6f7f0; font: bold" href="${pageContext.request.contextPath}/user-login/to-login-page">账号登录</a>
                    </p>

                    <div class="input-group">
                        <input type="hidden" id="userName" name="userName" value="${userinfo.userName}"/>
                        <input type="password" class="form-control input-dark" name="userPassWord" id="userPassWord"
                               placeholder="Password" data-validate="minlength[6]"/>
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-primary"> 登 录 </button>
                        </span>
                    </div>
                </div>

            </form>

        </div>

    </div>

</div>
</body>
<script>
    window.history.forward(1);
    //防止页面后退
    history.pushState(null, null, document.URL);
    window.addEventListener('popstate', function () {
        history.pushState(null, null, document.URL);
    });
</script>
</html>