﻿<%--suppress JSUnresolvedFunction --%>
<%--suppress ALL --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SMP - 用户列表</title>

    <script src="${pageContext.request.contextPath}/assets/boot.js"></script>
    <script src="${pageContext.request.contextPath}/assets/pagejs/frame/content.js"></script>


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="${pageContext.request.contextPath}/assets/js/html5shiv/html5shiv.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/respond/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="page-container">


    <div class="panel panel-default" style="padding-top: 0px">

        <div class="panel-body">

            <!-- search -->
            <%--<div class="panel panel-flat panel-color panel-success collapsed">--%>
            <div class="panel panel-flat panel-color panel-info collapsed">
            <%--<div class="panel panel-flat panel-color panel-default collapsed">--%>
                <div class="panel-heading" style="padding: 10px 30px ; font-size: 15px">
                    <h3 class="panel-title">
                        <a href="#" data-toggle="panel">
                            <i class="linecons-search"></i>搜索
                        </a>
                    </h3>

                    <div class="panel-options">
                        <a href="#" data-toggle="panel">
                            <span class="collapse-icon">&ndash;</span>
                            <span class="expand-icon">+</span>
                        </a>
                        <a href="#" data-toggle="remove">
                            &times;
                        </a>
                    </div>
                </div>

                <div class="panel-body">
                    <form role="form" id="search-form" method="post">

                        <div class="row">

                            <div class="col-xs-3">

                                <div class="form-group">
                                    <label class="control-label">账户</label>

                                    <input type="text" class="form-control" name="userName">
                                </div>

                            </div>

                            <div class="col-xs-3">

                                <div class="form-group">
                                    <label class="control-label">昵称</label>

                                    <input type="text" class="form-control" name="userNickName">
                                </div>

                            </div>

                            <div class="col-xs-3">

                                <div class="form-group">
                                    <label class="control-label">手机号码</label>
                                    <input type="text" class="form-control" name="userPhone">
                                </div>

                            </div>

                            <div class="col-xs-3">

                                <div class="form-group">
                                    <label for="status" class="control-label">状态</label>
                                    <script type="text/javascript">
                                        jQuery(document).ready(function ($) {
                                            $("#search_status").select2({
                                                placeholder: '请选择',
                                                allowClear: true
                                            }).on('select2-open', function () {
                                                // Adding Custom Scrollbar
                                                $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
                                            });
                                        });
                                    </script>
                                    <select class="form-control" id="search_status" name="status">
                                        <option></option>
                                        <option value="Y">有效</option>
                                        <option value="N">无效</option>
                                    </select>
                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12" style="text-align: right;">
                                <button type="button" class="btn btn-info btn-primary" id="search-button">
                                    <i class="fa-search"></i>
                                    搜 索
                                </button>
                                <button type="reset" class="btn btn-white"> 重 置</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer"></div>
            <!-- search end ..-->

            <!-- Table 工具栏 -->
            <div style="width: 120px">
                <div id="toolbar" class="btn-group">
                    <a type="button" class="btn btn-white btn-sm linecons-user" id="user-new"> 新 增 </a>
                    <a type="button" class="btn btn-white btn-sm linecons-pencil" id="user-edit"> 编 辑 </a>
                    <a type="button" class="btn btn-white btn-sm linecons-lock" id="user-lock"> 锁 定 </a>
                    <a type="button" class="btn btn-white btn-sm linecons-eye" id="user-unlock"> 解 锁 </a>
                </div>

            </div>
            <table id="user-list-table"></table>
        </div>

    </div>

    <!-- Modal 用户编辑-->
    <div class="modal fade" id="user-info-modal" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">


                <div class="modal-header" style="border-bottom: none;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>


                <div class="modal-body">
                    <form role="form" id="user-edit-form" method="post" class="validate">

                        <input type="text" name="uuid" class="hidden-input"/>
                        <input type="text" name="createTime" class="hidden-input"/>
                        <input type="text" name="password" class="hidden-input"/>
                        <div class="form-group">
                            <label class="control-label">帐号</label>

                            <input type="text" class="form-control" name="account" data-validate="required"
                                   maxlength="20" readonly/>
                        </div>

                        <div class="form-group">
                            <label class="control-label">昵称</label>

                            <input type="text" class="form-control" name="name" data-validate="required"
                                   placeholder="请输入用户昵称" maxlength="20"/>
                        </div>

                        <div class="form-group">
                            <label class="control-label">手机号码</label>
                            <input type="text" class="form-control" name="mobile"
                                   data-validate="required,minlength[11]"
                                   data-mask="phone" placeholder="请输入手机号码"/>
                        </div>

                        <div class="form-group">
                            <label class="control-label">所属部门</label>

                            <script type="text/javascript">
                                jQuery(document).ready(function ($) {
                                    $("#s2example-2").select2({
                                        allowClear: true
                                    }).on('select2-open', function () {
                                        // Adding Custom Scrollbar
                                        $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
                                    });
                                });
                            </script>

                            <select class="form-control" id="s2example-2" multiple required placeholder="请选择所属部门" autocomplete="off">
                                <option></option>
                                <optgroup label="United States">
                                    <option>Alabama</option>
                                    <option>Alaska</option>
                                    <option>Arizona</option>
                                    <option>Arkansas</option>
                                    <option>California</option>
                                    <option>Colorado</option>
                                    <option>Connecticut</option>
                                </optgroup>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label">状态</label>
                            <script type="text/javascript">
                                jQuery(document).ready(function ($) {
                                    $("#status").select2({
                                        allowClear: true
                                    }).on('select2-open', function () {
                                        // Adding Custom Scrollbar
                                        $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
                                    });
                                });
                            </script>
                            <select class="form-control" id="status" name="status" required placeholder="请选择状态">
                                <option value="Y">有效</option>
                                <option value="N">无效</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label">电子邮件</label>

                            <input type="text" class="form-control" name="email" data-validate="email"
                                   placeholder="请输入电子邮箱" maxlength="30"/>
                        </div>

                        <div class="form-group">
                            <label class="control-label">备注</label>
                            <textarea class="form-control autogrow" placeholder="" name="mark" rows="4"
                                      maxlength="100"></textarea>
                        </div>

                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-success">
                        <i class="fa-check"></i>
                        <span> 提 交 </span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal 用户编辑 end-->
</div>
<div class="page-loading-overlay">
    <div class="loader-2"></div>
</div>
<script src="${pageContext.request.contextPath}/assets/pagejs/user-manager/user-list.js"></script>
</body>
</html>