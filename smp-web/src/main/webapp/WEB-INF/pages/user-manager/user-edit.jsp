<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SMP - 用户新增</title>

    <script src="${pageContext.request.contextPath}/assets/boot.js"></script>
    <script src="${pageContext.request.contextPath}/assets/pagejs/frame/content.js"></script>
    <script src="${pageContext.request.contextPath}/assets/pagejs/user-manager/user-edit.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="${pageContext.request.contextPath}/assets/js/html5shiv/html5shiv.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/respond/respond.min.js"></script>
    <![endif]-->
</head>
<body>


<div class="page-container">

    <div class="panel panel-default">

        <div class="panel-heading">
            <div class="panel-title">
                创建新的用户
            </div>

            <small class="text-small pull-right" style="padding-top:5px;">
                <code>用户初始化密码为: 666666</code>
            </small>

        </div>

        <div class="panel-body">

            <form role="form" id="user-edit-form" method="post" class="validate">

                <div class="form-group">
                    <label class="control-label">帐号</label>

                    <input type="text" class="form-control" name="account" data-validate="required"
                           placeholder="请输入用户名" maxlength="20"/>
                </div>

                <div class="form-group">
                    <label class="control-label">昵称</label>

                    <input type="text" class="form-control" name="name" data-validate="required"
                           placeholder="请输入用户昵称" maxlength="20"/>
                </div>

                <div class="form-group">
                    <label class="control-label">手机号码</label>
                    <input type="text" class="form-control" name="mobile" data-validate="required,minlength[11]"
                           data-mask="phone" placeholder="请输入手机号码"/>
                </div>

                <div class="form-group">
                    <label class="control-label">所属部门</label>

                    <script type="text/javascript">
                        jQuery(document).ready(function ($) {
                            $("#s2example-2").select2({
                                allowClear: true
                            }).on('select2-open', function () {
                                // Adding Custom Scrollbar
                                $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
                            });
                        });
                    </script>

                    <select class="form-control" id="s2example-2" multiple required placeholder="请选择所属部门">
                        <option></option>
                        <optgroup label="United States">
                            <option>Alabama</option>
                            <option>Alaska</option>
                            <option>Arizona</option>
                            <option>Arkansas</option>
                            <option>California</option>
                            <option>Colorado</option>
                            <option>Connecticut</option>
                        </optgroup>
                    </select>
                </div>

                <div class="form-group">
                    <label class="control-label">电子邮件</label>

                    <input type="text" class="form-control" name="email" data-validate="email"
                           placeholder="请输入电子邮箱" maxlength="30"/>
                </div>

                <div class="form-group">
                    <label class="control-label">备注</label>
                    <textarea class="form-control autogrow" placeholder="" name="mark" rows="4"
                              maxlength="100"></textarea>
                </div>


                <div class="form-group">
                    <button type="button" class="btn btn-success">
                        <i class="fa-check"></i>
                        <span> 提 交 </span>
                    </button>
                    <button type="reset" class="btn btn-white"> 重 置 </button>
                </div>

            </form>

        </div>

    </div>

</div>

<div class="page-loading-overlay">
    <div class="loader-2"></div>
</div>

</body>
</html>