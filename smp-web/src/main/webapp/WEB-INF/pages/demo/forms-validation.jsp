<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Form Validation</title>

    <script src="${pageContext.request.contextPath}/assets/boot.js"></script>
    <script src="${pageContext.request.contextPath}/assets/pagejs/frame/content.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="${pageContext.request.contextPath}/assets/js/html5shiv/html5shiv.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/respond/respond.min.js"></script>
    <![endif]-->
</head>
<body>


<div class="page-container">


    <div class="panel panel-default">

        <div class="panel-heading">
            <div class="panel-title">
                输入框验证
            </div>

            <small class="text-small pull-right" style="padding-top:5px;">
                <code>用户初始化密码为: 666666</code>
            </small>
        </div>

        <div class="panel-body">

            <form role="form" id="form1" method="post" class="validate">

                <div class="form-group">
                    <label class="control-label">必需字段+自定义消息</label>

                    <input type="text" class="form-control" name="name" data-validate="required"
                           placeholder="Required Field"/>
                </div>

                <div class="form-group">
                    <label class="control-label">电子邮件</label>

                    <input type="text" class="form-control" name="email" data-validate="email"
                           placeholder="Email Field"/>
                </div>

                <div class="form-group">
                    <label class="control-label">Input Min Field</label>

                    <input type="text" class="form-control" name="min_field" data-validate="number,minlength[4]"
                           placeholder="Numeric + Minimun Length Field"/>
                </div>

                <div class="form-group">
                    <label class="control-label">Input Max Field</label>

                    <input type="text" class="form-control" name="max_field" data-validate="maxlength[2]"
                           placeholder="Maximum Length Field"/>
                </div>

                <div class="form-group">
                    <label class="control-label">Numeric Field</label>

                    <input type="text" class="form-control" name="number" data-validate="number"
                           placeholder="Numeric Field"/>
                </div>

                <div class="form-group">
                    <label class="control-label">URL Field</label>

                    <input type="text" class="form-control" name="url" data-validate="required,url"
                           placeholder="URL"/>
                </div>

                <div class="form-group">
                    <label class="control-label">Credit Card Field</label>

                    <input type="text" class="form-control" name="creditcard" data-validate="required,creditcard"
                           placeholder="Credit Card"/>
                </div>

                <div class="form-group">
                    <label class="control-label">phone</label>

                    <input type="text" class="form-control" name="phone" data-validate="required"
                           placeholder="phone"/>
                </div>

                <div class="form-group">
                    <label class="control-label">所属部门</label>

                    <script type="text/javascript">
                        jQuery(document).ready(function ($) {
                            var data = [{id: 0, text: 'enhancement'}, {id: 1, text: 'bug'}, {
                                id: 2,
                                text: 'duplicate'
                            }, {id: 3, text: 'invalid'}, {id: 4, text: 'wontfix'}];
                            $("#user_groups").select2({
                                data: data,
                                placeholder: '请选择',
                                allowClear: true,
                                multiple: true
                            }).on('select2-open', function () {
                                $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
                            });
                        });
                    </script>

                    <input type="text" class="form-control" id="user_groups" data-validate="required" multiple="multiple" required/>
                </div>

                <div class="form-group">
                    <label class="control-label">所属部门</label>

                    <script type="text/javascript">
                        jQuery(document).ready(function ($) {
                            $("#user_groups2").select2({
                                placeholder: '请选择',
                                allowClear: true
                            }).on('select2-open', function () {
                                $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
                            });
                        });
                    </script>

                    <!--<input type="text" class="form-control" id="user_groups2" data-validate="required" multiple="multiple" required/>-->
                    <select class="form-control" id="user_groups2" data-validate="required" required>
                        <option selected></option>
                        <option value="1">test</option>
                        <option value="2">test</option>
                    </select>
                </div>


                <div class="form-group">
                    <button type="submit" class="btn btn-success">Validate</button>
                    <button type="reset" class="btn btn-white">Reset</button>
                </div>

            </form>

        </div>

    </div>

</div>

<div class="page-loading-overlay">
    <div class="loader-2"></div>
</div>

</body>
</html>