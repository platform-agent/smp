﻿<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Form Validation</title>

    <script src="${pageContext.request.contextPath}/assets/boot.js"></script>
    <script src="${pageContext.request.contextPath}/assets/pagejs/frame/content.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="${pageContext.request.contextPath}/assets/js/html5shiv/html5shiv.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/respond/respond.min.js"></script>
    <![endif]-->
</head>
<body>


<div class="page-container">


    <div class="panel panel-default">

        <div class="panel-body">

            <div id="toolbar" class="btn-group">
                <button id="btn_add" type="button" class="btn btn-default">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>新增
                </button>
                <button id="btn_edit" type="button" class="btn btn-default">
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>修改
                </button>
                <button id="btn_delete" type="button" class="btn btn-default">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>删除
                </button>
            </div>
            <table id="tb_departments"></table>
        </div>

    </div>
</div>
<script>

    $(function () {
        //1.初始化Table
        var url = contextPath + '/user/list';//请求后台的URL（*）
        var columns = [
            {
                checkbox: true
            },
            {
                field: 'userName',
                title: '用户名'
            },
            {
                field: 'userNickName',
                title: '用户昵称'
            },
            {
                field: 'userPhone',
                title: '手机号码'
            },
            {
                field: 'userMail',
                title: '电子邮箱'
            },{
                field: 'status',
                title: '有效状态',
                align: 'center',
                formatter: formatter.status
            },{
                field: 'regTime',
                title: '注册时间',
                align: 'center',
                formatter : formatter.date
            },{
                field: 'groupId',
                title: '所属部门'
            },{
                field: 'mark',
                title: '备注'
            }
        ];
        $('#tb_departments').SimpleTableInit(null, url, columns);


        //2.初始化Button的点击事件
        var oButtonInit = new ButtonInit();
        oButtonInit.Init();

    });

    var ButtonInit = function () {
        var oInit = new Object();
        var postdata = {};

        oInit.Init = function () {
            //$("#btn_add").click(function () {
            //    $("#myModalLabel").text("新增");
            //    $("#myModal").find(".form-control").val("");
            //    $('#myModal').modal()

            //    postdata.DEPARTMENT_ID = "";
            //});

            //$("#btn_edit").click(function () {
            //    var arrselections = $("#tb_departments").bootstrapTable('getSelections');
            //    if (arrselections.length > 1) {
            //        toastr.warning('只能选择一行进行编辑');

            //        return;
            //    }
            //    if (arrselections.length <= 0) {
            //        toastr.warning('请选择有效数据');

            //        return;
            //    }
            //    $("#myModalLabel").text("编辑");
            //    $("#txt_departmentname").val(arrselections[0].DEPARTMENT_NAME);
            //    $("#txt_parentdepartment").val(arrselections[0].PARENT_ID);
            //    $("#txt_departmentlevel").val(arrselections[0].DEPARTMENT_LEVEL);
            //    $("#txt_statu").val(arrselections[0].STATUS);

            //    postdata.DEPARTMENT_ID = arrselections[0].DEPARTMENT_ID;
            //    $('#myModal').modal();
            //});

            //$("#btn_delete").click(function () {
            //    var arrselections = $("#tb_departments").bootstrapTable('getSelections');
            //    if (arrselections.length <= 0) {
            //        toastr.warning('请选择有效数据');
            //        return;
            //    }

            //    Ewin.confirm({ message: "确认要删除选择的数据吗？" }).on(function (e) {
            //        if (!e) {
            //            return;
            //        }
            //        $.ajax({
            //            type: "post",
            //            url: "/Home/Delete",
            //            data: { "": JSON.stringify(arrselections) },
            //            success: function (data, status) {
            //                if (status == "success") {
            //                    toastr.success('提交数据成功');
            //                    $("#tb_departments").bootstrapTable('refresh');
            //                }
            //            },
            //            error: function () {
            //                toastr.error('Error');
            //            },
            //            complete: function () {

            //            }

            //        });
            //    });
            //});

            //$("#btn_submit").click(function () {
            //    postdata.DEPARTMENT_NAME = $("#txt_departmentname").val();
            //    postdata.PARENT_ID = $("#txt_parentdepartment").val();
            //    postdata.DEPARTMENT_LEVEL = $("#txt_departmentlevel").val();
            //    postdata.STATUS = $("#txt_statu").val();
            //    $.ajax({
            //        type: "post",
            //        url: "/Home/GetEdit",
            //        data: { "": JSON.stringify(postdata) },
            //        success: function (data, status) {
            //            if (status == "success") {
            //                toastr.success('提交数据成功');
            //                $("#tb_departments").bootstrapTable('refresh');
            //            }
            //        },
            //        error: function () {
            //            toastr.error('Error');
            //        },
            //        complete: function () {

            //        }

            //    });
            //});

            //$("#btn_query").click(function () {
            //    $("#tb_departments").bootstrapTable('refresh');
            //});
        };

        return oInit;
    };
</script>
</body>
</html>