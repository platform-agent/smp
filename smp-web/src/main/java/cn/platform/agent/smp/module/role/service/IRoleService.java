package cn.platform.agent.smp.module.role.service;

import cn.platform.agent.frame.core.common.ResultBean;
import cn.platform.agent.frame.core.dao.Page;
import cn.platform.agent.smp.pojo.Role;

import java.util.List;
import java.util.Map;

public interface IRoleService {

	/**
	 * @Title: save
	 * @Description: 保存 {@code role}对象，当UUID为空时，则插入数据； 插入数据时，需要判断{@code name}
	 *               是否唯一 当UUID不为{@code null}时，则修改数据
	 * @param role
	 *            void
	 */
    ResultBean save(Role role);

    /**
     * 将sys_role置为无效状态
     * @param role
     * @return
     */
    ResultBean lock(List<Role> role);

    /**
     * 将sys_role置为有效状态
     * @param role
     * @return
     */
    ResultBean unlock(List<Role> role);

	/**
	 * @Title: delete
	 * @Description: 删除{@code role}信息，根据{@code role.uuid}来删除
	 * @param uuid
	 * @Return void
	 */
    ResultBean delete(String uuid);

	ResultBean getRole(String uuid);

	Page<Role> getRoleByPage(int page, int pageSize, Map<String, Object> params);

}