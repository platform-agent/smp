package cn.platform.agent.smp.module.portal.controller;

import cn.platform.agent.frame.core.common.ResultBean;
import cn.platform.agent.frame.core.common.ResultBeanHelper;
import cn.platform.agent.frame.core.helper.StringHelper;
import cn.platform.agent.smp.component.CommonComponents;
import cn.platform.agent.smp.component.ControllerExtend;
import cn.platform.agent.smp.pojo.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/portal")
public class PortalController extends ControllerExtend {

    //注入默认密码
    @Value("#{config['default.password']}")
    private String defaultPassword;

    /**
     * 跳转至首页
     * @return
     */
    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public ModelAndView home() {
        ModelAndView model = new ModelAndView("frame/main");
        User userInfo = this.getCurrentLoginUser();
        model.addObject("isInitializePass", false);//是否为初始化密码
        if (StringHelper.equals(CommonComponents.encryptPassword(defaultPassword) , userInfo.getPassword())) {
            model.addObject("isInitializePass", true);//为初始化密码
        }
        return model;
    }

    /**
     * 获取登录用户SESSION信息
     * @return
     */
    @RequestMapping(value = "/get-current-userinfo", method = RequestMethod.POST)
    @ResponseBody
    public ResultBean<User> getCurrentUserInfo() {
        User userInfo = this.getCurrentLoginUser();
        return ResultBeanHelper.success(userInfo);
    }
}
