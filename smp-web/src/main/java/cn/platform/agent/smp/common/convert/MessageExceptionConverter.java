package cn.platform.agent.smp.common.convert;

import cn.platform.agent.frame.core.common.MessageBean;
import cn.platform.agent.frame.core.common.ResultBeanHelper;
import cn.platform.agent.frame.core.config.PropertyPlaceholder;
import cn.platform.agent.frame.core.exception.*;
import cn.platform.agent.frame.core.helper.StringHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author Bob Lee
 * @ClassName: MessageExceptionConverter
 * @Description: 消息中心异常返回转换器
 * @date 2017年3月29日 下午3:59:38
 */
public class MessageExceptionConverter implements IExceptionConverter {

    private static final Logger logger = LoggerFactory.getLogger(MessageExceptionConverter.class);

    public static final String FAIL_CODE = "1";

    /**
     * @Fields SEPARATOR : 分隔符，用于信息提示
     */
    private static final String SEPARATOR = ";";


    @Autowired
    PropertyPlaceholder placeholder;

    @Override
    public Object convert(DaoException e) {
        return this.convertBaseException(e);
    }

    @Override
    public Object convert(ServiceException e) {
        return this.convertBaseException(e);
    }

    private Object convertBaseException(BaseException e) {
        MessageBean bean = e.getMessageBean();
        if (StringHelper.isNotEmpty(bean.getCode())) {
            String message = placeholder.getValue(bean.getCode());
            bean.setTemplate(message);
            bean.setArgs(bean.getArgs());
        }
        return ResultBeanHelper.error(bean);
    }

    @Override
    public Object convert(ValidationException e) {
        List<MessageBean> mbList = e.getMessageBeans();
        for (MessageBean bean : mbList) {

            if (StringHelper.isEmpty(bean.getCode())) continue;

            String message = placeholder.getValue(bean.getCode());
            if (StringHelper.isEmpty(message)) {
                continue;
            }
            bean.setTemplate(message);
            bean.setArgs(bean.getArgs());
        }
        return ResultBeanHelper.error(mbList);
    }

    @Override
    public Object convert(WebException e) {
        return this.convertBaseException(e);
    }

    @Override
    public Object convert(ComponentException e) {
        return this.convertBaseException(e);
    }

    @Override
    public Object convert(Throwable e) {
        return ResultBeanHelper.error(new MessageBean("System.error", "未知错误：%s", new String[]{e.getMessage()}));
    }

}
