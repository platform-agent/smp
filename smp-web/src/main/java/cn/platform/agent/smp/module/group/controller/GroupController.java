package cn.platform.agent.smp.module.group.controller;

import cn.platform.agent.frame.core.common.ResultBean;
import cn.platform.agent.frame.core.common.ResultBeanHelper;
import cn.platform.agent.frame.core.dao.Page;
import cn.platform.agent.smp.module.group.service.IGroupService;
import cn.platform.agent.smp.pojo.Group;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping(value = "/group")
public class GroupController {

    private static final Logger logger = LoggerFactory.getLogger(GroupController.class);

    @Resource
    IGroupService groupServiceImpl;

    /**
     * 跳转分组列表页面
     *
     * @return
     */
    @RequestMapping(value = "/to-list-page", method = RequestMethod.GET)
    public String toListPage() {
        return "group-manager/group-list";
    }

    /**
     * 根据用户UUID查找部门信息
     *
     * @param User
     * @return
     */
    @RequestMapping(value = "/find-by-uuid", method = RequestMethod.POST)
    @ResponseBody
    public ResultBean findGroupByUUID(@RequestBody Group group) {
        logger.debug("findGroupByUUID[{}]", group.getUuid());
        return this.groupServiceImpl.getGroup(group.getUuid());
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public ResultBean save(@RequestBody Group group) {
        logger.debug("Group[{}]", group);
        return this.groupServiceImpl.save(group);
    }

    /**
     * 锁定部门
     *
     * @param groups
     * @return
     */
    @RequestMapping(value = "/lock", method = RequestMethod.POST)
    @ResponseBody
    public ResultBean lockUser(@RequestBody List<Group> groups) {
        logger.debug("锁定部门 Group[{}]", groups);
        return this.groupServiceImpl.lock(groups);
    }

    /**
     * 锁定部门
     *
     * @param groups
     * @return
     */
    @RequestMapping(value = "/unlock", method = RequestMethod.POST)
    @ResponseBody
    public ResultBean unlockUser(@RequestBody List<Group> groups) {
        logger.debug("锁定部门 Group[{}]", groups);
        return this.groupServiceImpl.unlock(groups);
    }

    @RequestMapping(value = "/load-tree-data", method = RequestMethod.POST)
    @ResponseBody
    public ResultBean loadTreeData() {
        return this.groupServiceImpl.loadTreeData(null);
    }

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ResponseBody
    public ResultBean<Page<Group>> findGroupByPage(@RequestBody Page<Group> page) {
        ResultBean<Page<Group>> resultBean = ResultBeanHelper.success(this.groupServiceImpl.getGroupByPage(page.getPage(), page.getPageSize(), page.getParams()));
        return resultBean;
    }

}