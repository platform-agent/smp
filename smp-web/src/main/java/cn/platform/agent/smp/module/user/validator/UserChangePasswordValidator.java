package cn.platform.agent.smp.module.user.validator;

import cn.platform.agent.frame.core.common.MessageBean;
import cn.platform.agent.frame.core.common.MessageBeanHelper;
import cn.platform.agent.frame.core.validate.IValidator;
import cn.platform.agent.frame.core.validate.model.ValidatorConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component("UserChangePasswordValidator")
public class UserChangePasswordValidator implements IValidator {

    private static final Logger logger = LoggerFactory.getLogger(UserChangePasswordValidator.class);


	@Override
	public List<MessageBean> validate(Object parameterValue, ValidatorConfig configBean, Map<String, Object> paras) {
        HashMap<String, Object> param = (HashMap<String, Object>) parameterValue;

        String password = (String) param.get("password");
        String oldPassword = (String) param.get("oldPassword");
        String newPassword = (String) param.get("newPassword");
        String confirmPassword = (String) param.get("confirmPassword");

        if (!password.equals(oldPassword)){
            MessageBean messageBean = new MessageBean("old.passwords.dont.match");
            return MessageBeanHelper.buildForOne(messageBean);
        }

        if (!newPassword.equals(confirmPassword)){
            MessageBean messageBean = new MessageBean("tow.password.does.not.match");
            return MessageBeanHelper.buildForOne(messageBean);
        }

        return Collections.emptyList();
	}

	@Override
	public List<MessageBean> validate(Object value) {
		return this.validate(value, null, null);
	}

}