package cn.platform.agent.smp.module.menu.service;

import cn.platform.agent.frame.core.common.ResultBean;
import cn.platform.agent.frame.core.dao.Page;
import cn.platform.agent.smp.pojo.Menu;

import java.util.List;
import java.util.Map;

public interface IMenuService {

	/**
	 * @Title: save
	 * @Description: 保存 {@code menu}对象，当UUID为空时，则插入数据； 插入数据时，需要判断{@code name}
	 *               是否唯一 当UUID不为{@code null}时，则修改数据
	 * @param menu
	 *            void
	 */
    ResultBean save(Menu menu);

    /**
     * 将sys_menu置为无效状态
     * @param menu
     * @return
     */
    ResultBean lock(List<Menu> menu);

    /**
     * 将sys_menu置为有效状态
     * @param menu
     * @return
     */
    ResultBean unlock(List<Menu> menu);

	/**
	 * @Title: delete
	 * @Description: 删除{@code menu}信息，根据{@code menu.uuid}来删除
	 * @param uuid
	 * @Return void
	 */
    ResultBean delete(String uuid);

	ResultBean getMenu(String uuid);

	Page<Menu> getMenuByPage(int page, int pageSize, Map<String, Object> params);

}