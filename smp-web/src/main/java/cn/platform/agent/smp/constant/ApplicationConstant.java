package cn.platform.agent.smp.constant;

/**
 * <p>标题: 应用常量属性类</p>
 * <p>功能描述: </p>
 * <p>创建时间：17/4/20 </p>
 * <p>作者：SIVEN</p>
 * <p>修改历史记录：</p>
 * ============================================================<br>
 */
public class ApplicationConstant {

    /**
     * DES加密方式
     */
    public static final String DES_PASSWORD = "smp-password";

    /**
     * 状态枚举类
     */
    public enum Status{
        /**
         * 有效
         */
        EFFECTIVE("Y" , "有效"),
        /**
         * 无效
         */
        INVALID("N" , "无效");

        public String code;
        public String value;

        private Status(String code, String value) {
            this.code = code;
            this.value = value;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

}
