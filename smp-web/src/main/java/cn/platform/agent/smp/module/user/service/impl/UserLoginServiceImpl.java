package cn.platform.agent.smp.module.user.service.impl;

import cn.platform.agent.frame.core.common.MessageBean;
import cn.platform.agent.frame.core.common.ResultBean;
import cn.platform.agent.frame.core.common.ResultBeanHelper;
import cn.platform.agent.frame.core.helper.DesHelper;
import cn.platform.agent.frame.core.helper.InetHelper;
import cn.platform.agent.frame.core.helper.StringHelper;
import cn.platform.agent.frame.core.service.ServiceAssert;
import cn.platform.agent.smp.constant.ApplicationConstant;
import cn.platform.agent.smp.module.user.dao.UserDao;
import cn.platform.agent.smp.module.user.service.IUserLoginService;
import cn.platform.agent.smp.pojo.User;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;


@Service("userLoginServiceImpl")
public class UserLoginServiceImpl implements IUserLoginService {

    /**
     * @Fields UUID_IS_EMPTY : 传入的UUID为空
     */
    private static final String UUID_IS_EMPTY = "uuid.is.empty";

    @Resource
    private UserDao userDao;


    @Override
    public ResultBean login(String account, String password) {
        /**
         * 校验用户名或密码不能为空
         */
        ServiceAssert.assertTrue(StringHelper.isNotEmpty(account), new MessageBean("account.must.notnull"));
        ServiceAssert.assertTrue(StringHelper.isNotEmpty(password), new MessageBean("password.must.notnull"));

        //根据用户名获取账户信息
        User user = this.userDao.findByAccount(account);

        //用户不存在
        ServiceAssert.assertTrue(null != user, new MessageBean("user.does.not.exist"));

        //账户被锁定
        ServiceAssert.assertTrue(user.getStatus().equals(ApplicationConstant.Status.EFFECTIVE.getCode()), new MessageBean("account.is.locked"));

        //比较密码是否一致
        String desPassword = "";
        try {
            DesHelper des = new DesHelper(ApplicationConstant.DES_PASSWORD);
            desPassword = des.encrypt(password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //密码不一致
        ServiceAssert.assertTrue(user.getPassword().equals(desPassword), new MessageBean("account.password.isnot.consistent"));

        //更新最后登录时间
        user.setLastLoginIp(InetHelper.getIp());
        user.setLastLoginTime(new Date());
        userDao.update(user);

        return ResultBeanHelper.success(user);
    }

}