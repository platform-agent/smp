package cn.platform.agent.smp.module.menu.service.impl;

import cn.platform.agent.frame.core.common.MessageBean;
import cn.platform.agent.frame.core.common.ResultBean;
import cn.platform.agent.frame.core.common.ResultBeanHelper;
import cn.platform.agent.frame.core.dao.Page;
import cn.platform.agent.frame.core.helper.StringHelper;
import cn.platform.agent.frame.core.helper.UUIDHelper;
import cn.platform.agent.frame.core.service.ServiceAssert;
import cn.platform.agent.frame.core.validate.IValidator;
import cn.platform.agent.smp.module.menu.dao.MenuDao;
import cn.platform.agent.smp.module.menu.service.IMenuService;
import cn.platform.agent.smp.pojo.Menu;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("menuServiceImpl")
public class MenuServiceImpl implements IMenuService {

    /**
     * @Fields UUID_IS_EMPTY : 传入的UUID为空
     */
    private static final String UUID_IS_EMPTY = "uuid.is.empty";

    @Resource
    private MenuDao menuDao;

    @Resource
    @Qualifier(value = "HibernateBeanValidator")
    private IValidator hibernatebeanValidator;

    @Resource
    @Qualifier(value = "menuUniqueValidator")
    private IValidator menuUniqueValidator;

    @Override
    public ResultBean save(Menu menu) {
        if (menu.getUuid() == null) {
            menu.setUuid(UUIDHelper.getUuid());
            menu.setCreateTime(new Date());//
        }
        ServiceAssert.assertMessages(this.hibernatebeanValidator.validate(menu));
        ServiceAssert.assertMessages(this.menuUniqueValidator.validate(menu));
        this.menuDao.insertOrUpdate(menu);
        return ResultBeanHelper.success(menu);
    }

    @Override
    public ResultBean delete(String uuid) {
        ServiceAssert.assertTrue(StringHelper.isNotEmpty(uuid), new MessageBean(UUID_IS_EMPTY, MessageBean.buildArgs(uuid)));
        this.menuDao.delete(uuid);
        return ResultBeanHelper.success();
    }

    @Override
    public ResultBean getMenu(String uuid) {
        ServiceAssert.assertTrue(StringHelper.isNotEmpty(uuid), new MessageBean(UUID_IS_EMPTY, MessageBean.buildArgs(uuid)));
        Menu menu = this.menuDao.findById(uuid);
        ServiceAssert.assertTrue(menu != null, new MessageBean("find.menu.isnull"));
        return ResultBeanHelper.success(menu);
    }

    @Override
    public ResultBean lock(List<Menu> menus) {
        ServiceAssert.assertTrue(null != menus && menus.size() > 0, new MessageBean("锁定失败 ! 原因: 请求参数异常!"));
        List<String> uuids = new ArrayList<>();
        for (Menu temp : menus) {
            uuids.add(String.valueOf(temp.getUuid()));
        }
        this.menuDao.lockBatch(uuids);
        return ResultBeanHelper.success();
    }

    @Override
    public ResultBean unlock(List<Menu> menus) {
        ServiceAssert.assertTrue(null != menus && menus.size() > 0, new MessageBean("解锁失败 ! 原因: 请求参数异常!"));
        List<String> uuids = new ArrayList<>();
        for (Menu temp : menus) {
            uuids.add(String.valueOf(temp.getUuid()));
        }
        this.menuDao.unlockBatch(uuids);
        return ResultBeanHelper.success();
    }

    @Override
    public Page<Menu> getMenuByPage(int page, int pageSize, Map<String, Object> params) {
        Page<Menu> pageMenu = new Page<>(pageSize, page);
        pageMenu.setParams(params);
        this.menuDao.findAllByPage(pageMenu);
        return pageMenu;
    }

}