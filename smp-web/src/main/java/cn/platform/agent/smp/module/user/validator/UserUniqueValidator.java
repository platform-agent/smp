package cn.platform.agent.smp.module.user.validator;

import cn.platform.agent.frame.core.common.MessageBean;
import cn.platform.agent.frame.core.common.MessageBeanHelper;
import cn.platform.agent.frame.core.validate.IValidator;
import cn.platform.agent.frame.core.validate.model.ValidatorConfig;
import cn.platform.agent.smp.module.user.dao.UserDao;
import cn.platform.agent.smp.pojo.User;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Component("UserUniqueValidator")
public class UserUniqueValidator implements IValidator {

	private static final String USER_NAME_ALREADY_EXIST = "userInfo.name.already.exist";

	@Resource
	private UserDao userDao;

	@Override
	public List<MessageBean> validate(Object parameterValue, ValidatorConfig configBean, Map<String, Object> paras) {
        User user = (User) parameterValue;
        if(null ==user.getUuid()){
            if(this.userDao.countByAccount(user.getAccount()) > 0){
                MessageBean messageBean = new MessageBean(USER_NAME_ALREADY_EXIST , MessageBean.buildArgs(user.getAccount()));
                return MessageBeanHelper.buildForOne(messageBean);
            }
        }
        return Collections.emptyList();
	}

	@Override
	public List<MessageBean> validate(Object value) {
		return this.validate(value, null, null);
	}

}