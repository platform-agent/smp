package cn.platform.agent.smp.module.user.controller;


import cn.platform.agent.frame.core.common.ResultBean;
import cn.platform.agent.frame.core.common.ResultBeanHelper;
import cn.platform.agent.frame.core.dao.Page;
import cn.platform.agent.smp.component.ControllerExtend;
import cn.platform.agent.smp.module.user.service.IUserService;
import cn.platform.agent.smp.pojo.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping(value = "/user")
public class UserController extends ControllerExtend {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Resource
    IUserService UserServiceImpl;

    /**
     * 跳转用户编辑页面
     * @return
     */
    @RequestMapping(value = "/to-edit-page", method = RequestMethod.GET)
    public String toUserEditPage() {
        return "user-manager/user-edit";
    }

    /**
     * 跳转用户列表页面
     * @return
     */
    @RequestMapping(value = "/to-list-page", method = RequestMethod.GET)
    public String toUserListPage() {
        return "user-manager/user-list";
    }


    /**
     * 保存用户信息
     * @param User
     * @return
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public ResultBean save(@RequestBody User user) {
        logger.debug("保存用户信息 user[{}]", user);
        return this.UserServiceImpl.save(user);
    }

    /**
     * 重置密码
     * @param User
     * @return
     */
    @RequestMapping(value = "/reset-password", method = RequestMethod.POST)
    @ResponseBody
    public ResultBean resetPassword(@RequestBody User User) {
        logger.debug("重置密码 User[{}]", User);
        return this.UserServiceImpl.resetPassword(String.valueOf(User.getUuid()));
    }


    /**
     * 锁定账户
     * @param User
     * @return
     */
    @RequestMapping(value = "/lock", method = RequestMethod.POST)
    @ResponseBody
    public ResultBean lock(@RequestBody List<User> users) {
        logger.debug("锁定账户 User[{}]", users);
        return this.UserServiceImpl.lock(users);
    }

    /**
     * 锁定账户
     * @param User
     * @return
     */
    @RequestMapping(value = "/unlock", method = RequestMethod.POST)
    @ResponseBody
    public ResultBean unlock(@RequestBody List<User> users) {
        logger.debug("锁定账户 User[{}]", users);
        return this.UserServiceImpl.unlock(users);
    }

    /**
     * 密码修改
     * @param param
     * @return
     */
    @RequestMapping(value = "/change-password", method = RequestMethod.POST)
    @ResponseBody
    public ResultBean changePassword(@RequestBody HashMap<String , Object> param){
        logger.debug("密码修改 [{}]", param);
        User user = this.getCurrentLoginUser();
        param.put("uuid" , user.getUuid());
        ResultBean resultBean = this.UserServiceImpl.changePassword(param);
        if (resultBean.isSuccess()){
            user = (User)resultBean.getResultData();
            this.session().setAttribute(LOGIN_SESSION_KEY, user);
        }
        return resultBean;
    }

    /**
     * 根据用户UUID查找用户信息
     *
     * @param User
     * @return
     */
    @RequestMapping(value = "/find-by-uuid", method = RequestMethod.POST)
    @ResponseBody
    public ResultBean findUserByUUID(@RequestBody User user) {
        logger.debug("findUserByUUID[{}]", user.getUuid());
        return this.UserServiceImpl.getUser(user.getUuid().toString());
    }

    /**
     * 查询用户列表
     *
     * @param page
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ResponseBody
    public ResultBean<Page<User>> findUserByPage(@RequestBody Page<User> page) {
        Page<User> UserByPage = this.UserServiceImpl.getUserByPage(page.getPage(), page.getPageSize(), page.getParams());
        ResultBean<Page<User>> resultBean = ResultBeanHelper.success(UserByPage);
        return resultBean;
    }

}