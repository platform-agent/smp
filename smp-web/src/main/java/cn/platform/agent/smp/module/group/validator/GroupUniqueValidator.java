package cn.platform.agent.smp.module.group.validator;

import cn.platform.agent.frame.core.common.MessageBean;
import cn.platform.agent.frame.core.common.MessageBeanHelper;
import cn.platform.agent.frame.core.validate.IValidator;
import cn.platform.agent.frame.core.validate.model.ValidatorConfig;
import cn.platform.agent.smp.module.group.dao.GroupDao;
import cn.platform.agent.smp.pojo.Group;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Component("groupUniqueValidator")
public class GroupUniqueValidator implements IValidator {

    private static final String GROUP_CODE_ALREADY_EXIST = "group.code.already.exist";
    private static final String GROUP_NAME_ALREADY_EXIST = "group.name.already.exist";

    @Resource
    private GroupDao groupDao;

    @Override
    public List<MessageBean> validate(Object parameterValue, ValidatorConfig configBean, Map<String, Object> paras) {
        Group group = (Group) parameterValue;
        //部门编码唯一
        if (this.groupDao.countByCode(group.getCode(), group.getUuid()) > 0) {
            MessageBean messageBean = new MessageBean(GROUP_CODE_ALREADY_EXIST, MessageBean.buildArgs(group.getCode()));
            return MessageBeanHelper.buildForOne(messageBean);
        }

        //部门名称唯一
        if (this.groupDao.countByName(group.getName(), group.getUuid()) > 0) {
            MessageBean messageBean = new MessageBean(GROUP_NAME_ALREADY_EXIST, MessageBean.buildArgs(group.getName()));
            return MessageBeanHelper.buildForOne(messageBean);
        }
        return Collections.emptyList();
    }

    @Override
    public List<MessageBean> validate(Object value) {
        return this.validate(value, null, null);
    }

}