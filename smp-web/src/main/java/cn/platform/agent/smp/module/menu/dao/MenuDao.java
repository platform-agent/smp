package cn.platform.agent.smp.module.menu.dao;

import cn.platform.agent.frame.core.dao.Page;
import cn.platform.agent.smp.pojo.Menu;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * @ClassName: MenuDao
 * @Description: menu data table operate class
 * @author fuc
 * @date 2017年3月10日 下午10:39:56
 */
@MapperScan
public interface MenuDao {

	void insertOrUpdate(Menu menu);

	void insertBatch(List<Menu> menus);

	void delete(String uuid);

	void deleteBatch(List<String> uuids);

	Menu findById(String uuid);

	List<Menu> findAllByPage(Page page);

	int count();

	void update(Menu menu);

    void lockBatch(List<String> uuids);

    void unlockBatch(List<String> uuids);

}