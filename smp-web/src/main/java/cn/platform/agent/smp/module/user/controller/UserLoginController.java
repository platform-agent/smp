package cn.platform.agent.smp.module.user.controller;

import cn.platform.agent.frame.core.common.ResultBean;
import cn.platform.agent.smp.component.ControllerExtend;
import cn.platform.agent.smp.module.user.service.IUserLoginService;
import cn.platform.agent.smp.pojo.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

@Controller
@RequestMapping(value = "/user-login")
public class UserLoginController extends ControllerExtend {

    private static final Logger logger = LoggerFactory.getLogger(UserLoginController.class);

    @Resource
    IUserLoginService userLoginServiceImpl;

    /**
     * 跳转到登录页
     *
     * @return
     */
    @RequestMapping(value = "/to-login-page", method = RequestMethod.GET)
    public String toLoginPage() {
        return "login";
    }


    /**
     * 用户登录
     *
     * @param username
     * @param password
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public ResultBean login(@RequestBody User User) {
        ResultBean resultBean = userLoginServiceImpl.login(User.getAccount(), User.getPassword());
        if (resultBean.isSuccess()) {
            User = (User) resultBean.getResultData();
            this.session().setAttribute(LOGIN_SESSION_KEY, User);
        }
        return resultBean;
    }

    /**
     * 用户登出
     *
     * @return
     */
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ModelAndView logout() {
        User User = this.getCurrentLoginUser();

        ModelAndView modelAndView = new ModelAndView("lockscreen");
        modelAndView.addObject("User", User);

        //移除SESSION
        this.request().getSession().removeAttribute(LOGIN_SESSION_KEY);
        return modelAndView;
    }
}