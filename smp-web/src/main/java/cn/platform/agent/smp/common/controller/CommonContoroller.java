package cn.platform.agent.smp.common.controller;

import cn.platform.agent.smp.component.ControllerExtend;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/common")
public class CommonContoroller extends ControllerExtend{
    /**
     * 404页面
     * @return
     */
    @RequestMapping(value = "/to-404-page", method = RequestMethod.GET)
    public String to404Page() {
        return "error/404";
    }
}
