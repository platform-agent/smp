package cn.platform.agent.smp.component;

import cn.platform.agent.frame.core.helper.JsonHelper;
import cn.platform.agent.frame.web.controller.BaseController;
import cn.platform.agent.smp.pojo.User;
import org.springframework.beans.factory.annotation.Value;

/**
 * <p>标题: </p>
 * <p>功能描述: </p>
 * <p>创建时间：17/4/23 </p>
 * <p>作者：SIVEN</p>
 * <p>修改历史记录：</p>
 * ============================================================<br>
 */
public class ControllerExtend extends BaseController {

    @Value("#{config['login.sesseion.key']}")
    public String LOGIN_SESSION_KEY;

    /**
     * 获取当前登录的用户信息
     * @return
     */
    public User getCurrentLoginUser(){
        String users = (String) this.session().getAttribute(LOGIN_SESSION_KEY);
        User user = JsonHelper.parseObject(users ,User.class);
        return user;
    }
}
