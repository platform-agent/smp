package cn.platform.agent.smp.module.role.dao;

import cn.platform.agent.frame.core.dao.Page;
import cn.platform.agent.smp.pojo.Role;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * @ClassName: RoleDao
 * @Description: role data table operate class
 * @author fuc
 * @date 2017年3月10日 下午10:39:56
 */
@MapperScan
public interface RoleDao {

	void insertOrUpdate(Role role);

	void insertBatch(List<Role> roles);

	void delete(String uuid);

	void deleteBatch(List<String> uuids);

	Role findById(String uuid);

	List<Role> findAllByPage(Page page);

	int count();

	void update(Role role);

    void lockBatch(List<String> uuids);

    void unlockBatch(List<String> uuids);

}