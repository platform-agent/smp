package cn.platform.agent.smp.component;

import cn.platform.agent.frame.core.helper.DesHelper;
import cn.platform.agent.smp.constant.ApplicationConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 通用组件
 * Created by SIVEN on 17/5/13.
 */
public class CommonComponents {

    private static final Logger logger = LoggerFactory.getLogger(CommonComponents.class);

    /**
     * 密码加密工具
     * @param password
     * @return
     */
    public static String encryptPassword(String password){
        String desPassword = null;
        try {
            //密码加密
            DesHelper des = new DesHelper(ApplicationConstant.DES_PASSWORD);
            desPassword = des.encrypt(password);
        } catch (Exception e) {
            logger.error("初始化DesHelper异常!!", e);
        }
        return desPassword;
    }
}
