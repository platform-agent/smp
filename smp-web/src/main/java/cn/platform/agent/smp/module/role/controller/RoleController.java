package cn.platform.agent.smp.module.role.controller;

import cn.platform.agent.frame.core.common.ResultBean;
import cn.platform.agent.frame.core.common.ResultBeanHelper;
import cn.platform.agent.frame.core.dao.Page;
import cn.platform.agent.smp.component.ControllerExtend;
import cn.platform.agent.smp.module.role.service.IRoleService;
import cn.platform.agent.smp.pojo.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping(value = "/role")
public class RoleController extends ControllerExtend {

	private static final Logger logger = LoggerFactory.getLogger(RoleController.class);

	@Resource
	IRoleService roleServiceImpl;

    /**
     * 跳转列表页面
     * @return
     */
    @RequestMapping(value = "/to-list-page", method = RequestMethod.GET)
    public String toListPage() {
        return "role-manager/role-list";
    }

    /**
     * 根据UUID查找sys_role信息
     * @param role
     * @return
     */
    @RequestMapping(value = "/find-by-uuid", method = RequestMethod.POST)
    @ResponseBody
    public ResultBean findByUUID(@RequestBody Role role) {
        logger.debug("根据UUID查找role信息 Role[{}]", role);
        return this.roleServiceImpl.getRole(role.getUuid());
    }

    /**
     * 保存sys_role信息
     * @param role
     * @return
     */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public ResultBean save(@RequestBody Role role) {
        logger.debug("保存role信息 Role[{}]", role);
        return this.roleServiceImpl.save(role);
	}

    /**
    * 将sys_role置为无效状态
    * @param role
    * @return
    */
    @RequestMapping(value = "/lock", method = RequestMethod.POST)
    @ResponseBody
    public ResultBean lock(@RequestBody List<Role> role){
        logger.debug("将role置为无效状态 Role[{}]", role);
        return this.roleServiceImpl.lock(role);
    }

    /**
     * 将sys_role置为有效状态
    * @param role
    * @return
    */
    @RequestMapping(value = "/unlock", method = RequestMethod.POST)
    @ResponseBody
    public ResultBean unlock(@RequestBody List<Role> role){
        logger.debug("将role置为有效状态 Role[{}]", role);
        return this.roleServiceImpl.unlock(role);
    }

    /**
    * 获取列表数据
    * @param page
    * @return
    */
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@ResponseBody
	public ResultBean<Page<Role>> findRoleByPage(@RequestBody Page<Role> page) {
		ResultBean<Page<Role>> resultBean = ResultBeanHelper.success(this.roleServiceImpl.getRoleByPage(page.getPage(), page.getPageSize(), page.getParams()));
		return resultBean;
	}

}