package cn.platform.agent.smp.module.role.service.impl;

import cn.platform.agent.frame.core.common.MessageBean;
import cn.platform.agent.frame.core.common.ResultBean;
import cn.platform.agent.frame.core.common.ResultBeanHelper;
import cn.platform.agent.frame.core.dao.Page;
import cn.platform.agent.frame.core.helper.StringHelper;
import cn.platform.agent.frame.core.helper.UUIDHelper;
import cn.platform.agent.frame.core.service.ServiceAssert;
import cn.platform.agent.frame.core.validate.IValidator;
import cn.platform.agent.smp.module.role.dao.RoleDao;
import cn.platform.agent.smp.module.role.service.IRoleService;
import cn.platform.agent.smp.pojo.Role;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("roleServiceImpl")
public class RoleServiceImpl implements IRoleService {

	/**
	 * @Fields UUID_IS_EMPTY : 传入的UUID为空
	 */
	private static final String UUID_IS_EMPTY = "uuid.is.empty";

	@Resource
	private RoleDao roleDao;

	@Resource
	@Qualifier(value = "HibernateBeanValidator")
	private IValidator hibernatebeanValidator;

	@Resource
	@Qualifier(value = "roleUniqueValidator")
	private IValidator roleUniqueValidator;

	@Override
	public ResultBean save(Role role) {
		if (role.getUuid() == null) {
			role.setUuid(UUIDHelper.getUuid());
			role.setCreateTime(new Date());//
		}
        ServiceAssert.assertMessages(this.hibernatebeanValidator.validate(role));
        ServiceAssert.assertMessages(this.roleUniqueValidator.validate(role));
        this.roleDao.insertOrUpdate(role);
        return ResultBeanHelper.success(role);
	}

	@Override
	public ResultBean delete(String uuid) {
		ServiceAssert.assertTrue(StringHelper.isNotEmpty(uuid), new MessageBean(UUID_IS_EMPTY, MessageBean.buildArgs(uuid)));
		this.roleDao.delete(uuid);
        return ResultBeanHelper.success();
	}

	@Override
	public ResultBean getRole(String uuid) {
		ServiceAssert.assertTrue(StringHelper.isNotEmpty(uuid), new MessageBean(UUID_IS_EMPTY, MessageBean.buildArgs(uuid)));
        Role role = this.roleDao.findById(uuid);
        ServiceAssert.assertTrue(role != null, new MessageBean("find.role.isnull"));
        return ResultBeanHelper.success(role);
	}

    @Override
    public ResultBean lock(List<Role> roles) {
        ServiceAssert.assertTrue(null != roles && roles.size() > 0, new MessageBean("锁定失败 ! 原因: 请求参数异常!"));
        List<String> uuids = new ArrayList<>();
        for (Role temp : roles) {
            uuids.add(String.valueOf(temp.getUuid()));
        }
        this.roleDao.lockBatch(uuids);
        return ResultBeanHelper.success();
    }

    @Override
    public ResultBean unlock(List<Role> roles) {
        ServiceAssert.assertTrue(null != roles && roles.size() > 0, new MessageBean("锁定失败 ! 原因: 请求参数异常!"));
        List<String> uuids = new ArrayList<>();
        for (Role temp : roles) {
            uuids.add(String.valueOf(temp.getUuid()));
        }
        this.roleDao.unlockBatch(uuids);
        return ResultBeanHelper.success();
    }

    @Override
	public Page<Role> getRoleByPage(int page, int pageSize, Map<String, Object> params) {
		Page<Role> pageRole = new Page<>(pageSize, page);
		pageRole.setParams(params);
		this.roleDao.findAllByPage(pageRole);
		return pageRole;
	}

}