package cn.platform.agent.smp.module.user.service.impl;

import cn.platform.agent.frame.core.common.MessageBean;
import cn.platform.agent.frame.core.common.ResultBean;
import cn.platform.agent.frame.core.common.ResultBeanHelper;
import cn.platform.agent.frame.core.dao.Page;
import cn.platform.agent.frame.core.helper.StringHelper;
import cn.platform.agent.frame.core.helper.UUIDHelper;
import cn.platform.agent.frame.core.service.ServiceAssert;
import cn.platform.agent.frame.core.validate.IValidator;
import cn.platform.agent.smp.component.CommonComponents;
import cn.platform.agent.smp.constant.ApplicationConstant;
import cn.platform.agent.smp.module.user.dao.UserDao;
import cn.platform.agent.smp.module.user.service.IUserService;
import cn.platform.agent.smp.pojo.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;


@Service("UserServiceImpl")
public class UserServiceImpl implements IUserService {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    /**
     * @Fields UUID_IS_EMPTY : 传入的UUID为空
     */
    private static final String UUID_IS_EMPTY = "uuid.is.empty";

    @Resource
    private UserDao userDao;

    @Resource
    @Qualifier(value = "HibernateBeanValidator")
    private IValidator hibernatebeanValidator;

    @Resource
    @Qualifier(value = "UserUniqueValidator")
    private IValidator UserUniqueValidator;

    @Resource
    @Qualifier(value = "UserChangePasswordValidator")
    private IValidator UserChangePasswordValidator;

    //注入默认密码
    @Value("#{config['default.password']}")
    private String defaultPassword;

    @Override
    public ResultBean save(User user) {
        //设置默认密码
        if (StringHelper.isEmpty(user.getPassword())) {
            String desPassword = CommonComponents.encryptPassword(defaultPassword);
            user.setPassword(desPassword);
        }
        if (null == user.getUuid()) {
            user.setUuid(UUIDHelper.getUuid());
            user.setStatus(ApplicationConstant.Status.EFFECTIVE.getCode());//默认为有效账户
            user.setCreateTime(new Date());//注册日期
        }
        ServiceAssert.assertMessages(this.hibernatebeanValidator.validate(user));
        ServiceAssert.assertMessages(this.UserUniqueValidator.validate(user));
        this.userDao.insertOrUpdate(user);
        return ResultBeanHelper.success(user);
    }

    @Override
    public ResultBean resetPassword(String uuid) {
        //根据UUID获取用户信息
        ResultBean resultBean = getUser(uuid);
        User User = (User) resultBean.getResultData();
        //将密码重置为空, 调用save方法时, 将会重新初始化密码
        User.setPassword(null);
        return this.save(User);
    }

    @Override
    public ResultBean lock(List<User> users) {
        ServiceAssert.assertTrue(null != users && users.size() > 0, new MessageBean("锁定账户失败 ! 原因: 请求参数异常!"));
        List<String> uuids = new ArrayList<>();
        for (User User : users) {
            uuids.add(String.valueOf(User.getUuid()));
        }
        this.userDao.lockBatch(uuids);
        return ResultBeanHelper.success();
    }

    @Override
    public ResultBean unlock(List<User> users) {
        ServiceAssert.assertTrue(null != users && users.size() > 0, new MessageBean("解锁账户失败 ! 原因: 请求参数异常!"));
        List<String> uuids = new ArrayList<>();
        for (User User : users) {
            uuids.add(String.valueOf(User.getUuid()));
        }
        this.userDao.unlockBatch(uuids);
        return ResultBeanHelper.success();
    }


    @Override
    public ResultBean getUser(String uuid) {
        ServiceAssert.assertTrue(StringHelper.isNotEmpty(uuid), new MessageBean(UUID_IS_EMPTY, MessageBean.buildArgs(uuid)));
        User User = this.userDao.findById(uuid);
        ServiceAssert.assertTrue(User != null, new MessageBean("find.user.isnull"));
        return ResultBeanHelper.success(User);
    }

    @Override
    public Page<User> getUserByPage(int page, int pageSize, Map<String, Object> params) {
        Page<User> pageUser = new Page<>(pageSize, page);
        pageUser.setParams(params);
        this.userDao.findAllByPage(pageUser);
        return pageUser;
    }

    @Override
    public ResultBean changePassword(HashMap<String, Object> param) {
        String uuid = String.valueOf(param.get("uuid"));
        ResultBean resultBean = this.getUser(uuid);
        User User = (User) resultBean.getResultData();

        String passWord = User.getPassword();
        String oldPassword = (String) param.get("oldPassword");
        String newPassword = (String) param.get("newPassword");
        String confirmPassword = (String) param.get("confirmPassword");

        try {
            //密码加密
            oldPassword = CommonComponents.encryptPassword(oldPassword);
            newPassword = CommonComponents.encryptPassword(newPassword);
            confirmPassword = CommonComponents.encryptPassword(confirmPassword);
        } catch (Exception e) {
            logger.error("初始化DesHelper异常!!", e);
        }

        param.put("password" , passWord);
        param.put("oldPassword" , oldPassword);
        param.put("newPassword" , newPassword);
        param.put("confirmPassword" , confirmPassword);

        //校验修改的密码是否正确
        ServiceAssert.assertMessages(UserChangePasswordValidator.validate(param));

        User.setPassword(newPassword);
        return this.save(User);
    }

}