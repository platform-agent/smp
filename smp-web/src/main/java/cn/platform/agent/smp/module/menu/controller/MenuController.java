package cn.platform.agent.smp.module.menu.controller;

import cn.platform.agent.frame.core.common.ResultBean;
import cn.platform.agent.frame.core.common.ResultBeanHelper;
import cn.platform.agent.frame.core.dao.Page;
import cn.platform.agent.smp.component.ControllerExtend;
import cn.platform.agent.smp.module.menu.service.IMenuService;
import cn.platform.agent.smp.pojo.Menu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping(value = "/menu")
public class MenuController extends ControllerExtend {

	private static final Logger logger = LoggerFactory.getLogger(MenuController.class);

	@Resource
	IMenuService menuServiceImpl;

    /**
     * 跳转列表页面
     * @return
     */
    @RequestMapping(value = "/to-list-page", method = RequestMethod.GET)
    public String toListPage() {
        return "list";
    }

    /**
     * 根据UUID查找sys_menu信息
     * @param menu
     * @return
     */
    @RequestMapping(value = "/find-by-uuid", method = RequestMethod.POST)
    @ResponseBody
    public ResultBean findByUUID(@RequestBody Menu menu) {
        logger.debug("根据UUID查找menu信息 Menu[{}]", menu);
        return this.menuServiceImpl.getMenu(menu.getUuid());
    }

    /**
     * 保存sys_menu信息
     * @param menu
     * @return
     */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public ResultBean save(@RequestBody Menu menu) {
        logger.debug("保存menu信息 Menu[{}]", menu);
        return this.menuServiceImpl.save(menu);
	}

    /**
    * 将sys_menu置为无效状态
    * @param menu
    * @return
    */
    @RequestMapping(value = "/lock", method = RequestMethod.POST)
    @ResponseBody
    public ResultBean lock(@RequestBody List<Menu> menu){
        logger.debug("将menu置为无效状态 Menu[{}]", menu);
        return this.menuServiceImpl.lock(menu);
    }

    /**
     * 将sys_menu置为有效状态
    * @param groups
    * @return
    */
    @RequestMapping(value = "/unlock", method = RequestMethod.POST)
    @ResponseBody
    public ResultBean unlock(@RequestBody List<Menu> menu){
        logger.debug("将menu置为有效状态 Menu[{}]", menu);
        return this.menuServiceImpl.unlock(menu);
    }

    /**
    * 获取列表数据
    * @param groups
    * @return
    */
	@RequestMapping(value = "/page", method = RequestMethod.POST)
	@ResponseBody
	public ResultBean<Page<Menu>> findMenuByPage(@RequestBody Page<Menu> page) {
		ResultBean<Page<Menu>> resultBean = ResultBeanHelper.success(this.menuServiceImpl.getMenuByPage(page.getPage(), page.getPageSize(), page.getParams()));
		return resultBean;
	}

}