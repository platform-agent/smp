package cn.platform.agent.smp.module.group.service;

import cn.platform.agent.frame.core.common.ResultBean;
import cn.platform.agent.frame.core.dao.Page;
import cn.platform.agent.smp.pojo.Group;

import java.util.List;
import java.util.Map;

public interface IGroupService {

	/**
	 * @Title: save
	 * @Description: 保存 {@code group}对象，当UUID为空时，则插入数据； 插入数据时，需要判断{@code name}
	 *               是否唯一 当UUID不为{@code null}时，则修改数据
	 * @param group
	 *            void
	 */
    ResultBean save(Group group);

    ResultBean getGroup(String uuid);

	Page<Group> getGroupByPage(int page, int pageSize, Map<String, Object> params);

    /**
     * 加载树机构
     * @param group
     * @return
     */
    ResultBean loadTreeData(Group group);

    /**
     * 锁定分组, 置为无效状态
     * @param User
     * @return
     */
    ResultBean lock(List<Group> group);

    /**
     * 锁定分组, 置为无效状态
     * @param User
     * @return
     */
    ResultBean unlock(List<Group> group);
}