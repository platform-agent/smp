package cn.platform.agent.smp.module.user.service;

import cn.platform.agent.frame.core.common.ResultBean;

public interface IUserLoginService {

    /**
     * 用户登录
     * @param account
     * @param password
     * @return
     */
    public ResultBean login(String account, String password);

}