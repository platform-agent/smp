package cn.platform.agent.smp.module.role.validator;

import cn.platform.agent.frame.core.common.MessageBean;
import cn.platform.agent.frame.core.validate.IValidator;
import cn.platform.agent.frame.core.validate.model.ValidatorConfig;
import cn.platform.agent.smp.module.role.dao.RoleDao;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Component("roleUniqueValidator")
public class RoleUniqueValidator implements IValidator {

	private static final String USER_NAME_ALREADY_EXIST = "role.name.already.exist";

	@Resource
	private RoleDao roleDao;

	@Override
	public List<MessageBean> validate(Object parameterValue, ValidatorConfig configBean, Map<String, Object> paras) {

		return null;
	}

	@Override
	public List<MessageBean> validate(Object value) {
		return this.validate(value, null, null);
	}

}