package cn.platform.agent.smp.module.group.service.impl;

import cn.platform.agent.frame.core.common.*;
import cn.platform.agent.frame.core.dao.Page;
import cn.platform.agent.frame.core.helper.StringHelper;
import cn.platform.agent.frame.core.helper.UUIDHelper;
import cn.platform.agent.frame.core.service.ServiceAssert;
import cn.platform.agent.frame.core.validate.IValidator;
import cn.platform.agent.smp.module.group.dao.GroupDao;
import cn.platform.agent.smp.module.group.service.IGroupService;
import cn.platform.agent.smp.pojo.Group;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;



@Service("groupServiceImpl")
public class GroupServiceImpl implements IGroupService {

	/**
	 * @Fields UUID_IS_EMPTY : 传入的UUID为空
	 */
	private static final String UUID_IS_EMPTY = "uuid.is.empty";

	@Resource
	private GroupDao groupDao;

	@Resource
	@Qualifier(value = "HibernateBeanValidator")
	private IValidator hibernatebeanValidator;

	@Resource
	@Qualifier(value = "groupUniqueValidator")
	private IValidator groupUniqueValidator;

	String company = "科技有限公司";

	@Override
	public ResultBean save(Group group) {
		if (group.getUuid() == null) {
			group.setUuid(UUIDHelper.getUuid());
            group.setCreateTime(new Date());
		}
        ServiceAssert.assertMessages(this.hibernatebeanValidator.validate(group));
        ServiceAssert.assertMessages(this.groupUniqueValidator.validate(group));
        this.groupDao.insertOrUpdate(group);
        return ResultBeanHelper.success(group);
	}

	@Override
	public ResultBean getGroup(String uuid) {
		ServiceAssert.assertTrue(StringHelper.isNotEmpty(uuid), new MessageBean(UUID_IS_EMPTY, MessageBean.buildArgs(uuid)));
        Group group = this.groupDao.findById(uuid);
        ServiceAssert.assertTrue(group != null, new MessageBean("find.group.isnull"));
        if (StringHelper.isEmpty(group.getParentName())){
            group.setParentName(company);
        }
		return ResultBeanHelper.success(group);
	}

	@Override
	public Page<Group> getGroupByPage(int page, int pageSize, Map<String, Object> params) {
		Page<Group> pageGroup = new Page<>(pageSize, page);
		pageGroup.setParams(params);
		this.groupDao.findAllByPage(pageGroup);
		return pageGroup;
	}

    @Override
    public ResultBean loadTreeData(Group group) {
        List<Group> list = groupDao.findAll();
        if (CollectionUtils.isEmpty(list)){
            list = new ArrayList<>();
        }
        //封装顶级分组, 公司名字
        Group topGroup = new Group();
        topGroup.setUuid("0");
        topGroup.setName(company);
        topGroup.setPuuid(null);
        list.add(topGroup);

        TreeNodeBeanHelper.TreeOptions options = new TreeNodeBeanHelper.TreeOptions("uuid","puuid" , "name");
        List<TreeNodeBean> treeNodeBeans = TreeNodeBeanHelper.constructListDTOToTree(list, options);
        return ResultBeanHelper.success(treeNodeBeans);
    }

    @Override
    public ResultBean lock(List<Group> group) {
        ServiceAssert.assertTrue(null != group && group.size() > 0, new MessageBean("锁定失败 ! 原因: 请求参数异常!"));

        List<String> uuids = new ArrayList<>();
        for (Group groups : group) {
            uuids.add(String.valueOf(groups.getUuid()));
        }
        this.groupDao.lockBatch(uuids);
        return ResultBeanHelper.success();
    }

    @Override
    public ResultBean unlock(List<Group> group) {
        ServiceAssert.assertTrue(null != group && group.size() > 0, new MessageBean("解锁失败 ! 原因: 请求参数异常!"));

        List<String> uuids = new ArrayList<>();
        for (Group groups : group) {
            uuids.add(String.valueOf(groups.getUuid()));
        }
        this.groupDao.unlockBatch(uuids);
        return ResultBeanHelper.success();
    }

}