package cn.platform.agent.smp.module.user.service;

import cn.platform.agent.frame.core.common.ResultBean;
import cn.platform.agent.frame.core.dao.Page;
import cn.platform.agent.smp.pojo.User;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface IUserService {

	/**
	 * @Title: save
	 * @Description: 保存 {@code User}对象，当UUID为空时，则插入数据； 插入数据时，需要判断{@code name}
	 *               是否唯一 当UUID不为{@code null}时，则修改数据
	 * @param User
	 *            void
	 */
    ResultBean save(User user);

    /**
     * 重置密码
     * @param User
     * @return
     */
    ResultBean resetPassword(String uuid);

    /**
     * 锁定用户, 置为无效状态
     * @param User
     * @return
     */
    ResultBean lock(List<User> users);

    /**
     * 锁定用户, 置为无效状态
     * @param User
     * @return
     */
    ResultBean unlock(List<User> users);

    ResultBean getUser(String uuid);

	Page<User> getUserByPage(int page, int pageSize, Map<String, Object> params);

    /**
     * 修改密码
     * @param param
     * @return
     */
    public ResultBean changePassword(HashMap<String , Object> param);

}