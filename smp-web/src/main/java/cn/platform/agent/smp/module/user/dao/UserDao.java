package cn.platform.agent.smp.module.user.dao;

import cn.platform.agent.frame.core.dao.Page;
import cn.platform.agent.smp.pojo.User;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * @ClassName: UserDao
 * @Description: userInfo data table operate class
 * @author Bob Lee
 * @date 2017年3月10日 下午10:39:56
 */
@MapperScan
public interface UserDao {

	void insertOrUpdate(User user);

	void insertBatch(List<User> users);

    User findById(String uuid);

	List<User> findAllByPage(Page page);

	int count();

	void update(User user);

    int countByAccount(String account) ;

    User findByAccount(String account);

    void lockBatch(List<String> uuids);

    void unlockBatch(List<String> uuids);
}