package cn.platform.agent.smp.module.group.dao;

import cn.platform.agent.frame.core.dao.Page;
import cn.platform.agent.smp.pojo.Group;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * @ClassName: GroupDao
 * @Description: group data table operate class
 * @author Bob Lee
 * @date 2017年3月10日 下午10:39:56
 */
@MapperScan
public interface GroupDao {

	void insertOrUpdate(Group group);

	void insertBatch(List<Group> groups);

	Group findById(String uuid);

	List<Group> findAllByPage(Page page);

	List<Group> findAll();

	int count();

	int countByCode(@Param("code") String code , @Param("uuid")String uuid);

	int countByName(@Param("name") String name , @Param("uuid")String uuid);

	void update(Group group);

    void lockBatch(List<String> uuids);

    void unlockBatch(List<String> uuids);
}