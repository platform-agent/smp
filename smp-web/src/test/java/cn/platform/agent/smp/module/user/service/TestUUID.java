package cn.platform.agent.smp.module.user.service;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by SIVEN on 17/4/28.
 */
public class TestUUID {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    public String createUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }


    final AtomicInteger number = new AtomicInteger();
    volatile boolean bol = false;

    public static List<String> list = new ArrayList();

    final ConcurrentMap<String, String> map = new ConcurrentHashMap<>();

    @Test
    public void threadUUID() {
        //list = new ArrayList<>();
//        final HashMap<String, String> map = new HashMap<>();
        final int threadSize = 1000;
        final Thread[] thread = new Thread[threadSize];
        for (int i = 0; i < threadSize; i++) {
            thread[i] = new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int c = 0; c < threadSize*10; c++) {
                        String uuid = createUUID();
                        if (map.containsKey(uuid)) {
                            logger.error("UUID 重复啦!!!UUID: [{}]", uuid);
                            continue;
                        }
                        //logger.info("[{}]", uuid);
                        map.put(uuid, uuid);
                        logger.warn("Map Size:[{}]" , map.size());
                    }
                }

            });
            thread[i].start();
        }

        try {
            Thread.sleep(20000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
