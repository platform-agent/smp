package cn.platform.agent.smp.redis;

import cn.platform.agent.frame.core.redis.RedisClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * <p>标题: </p>
 * <p>功能描述: </p>
 * <p>创建时间：17/4/9 </p>
 * <p>作者：SIVEN</p>
 * <p>修改历史记录：</p>
 * ============================================================<br>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/spring-context-jedis.xml"})
public class RedisTest {

    Logger logger = LoggerFactory.getLogger(this.getClass());
    @Resource
    RedisTemplate redisTemplate;

    @Resource
    RedisClient redisClient;

    @Test
    public void set() {
        ValueOperations<String, List<String>> value = redisTemplate.opsForValue();
        List<String> listValue = new ArrayList<String>();
        listValue.add("001");
        listValue.add("002");
        value.set("list", listValue);
        System.out.println(value.get("list"));

    }

    @Test
    public void get(){
        redisClient.set("user:age", "18",10000L);
        System.out.println(redisClient.getObject("user:age"));
    }




    String key = "key";
    String[] str = new String[]{"1","1", "2", "3","4"};


    @Test
    public void deleteK(){
        for (String s : str) {
            redisClient.delete(s);
        }
    }


    @Test
    public void testIn() {
        deleteK();
        redisClient.delete(key);
        for (String s : str) {

            Long length = redisClient.length(key);
            if(length > 2){
                break;
            }

            boolean set = redisClient.set(s, s, 10L);
            if (set) {
                logger.error("成功[{}]" , s);
                redisClient.in(key, s);
            }else{
                logger.error("数据已经在redis中存在[{}]" , s);
            }
        }
        logger.info("队列一共有[{}]条记录" , redisClient.length(key));
    }

    @Test
    public void testOut() {

        System.out.println(Runtime.getRuntime().availableProcessors());
    }

    @Test
    public void testThreadOut() {
        testIn();
        ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
        for (int i = 0; i < str.length; i++) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    logger.error("redis value [{}]" , redisClient.out(key));
                    try {
                        Thread.sleep(500L);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            cachedThreadPool.execute(thread);
        }

        try {
            Thread.sleep(10000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
