package cn.platform.agent.smp.module.user.service;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by SIVEN on 17/4/28.
 */
public class ThreadUUID implements Runnable {

    Logger logger = LoggerFactory.getLogger(this.getClass());
    final AtomicInteger number = new AtomicInteger();
    volatile boolean bol = false;
    public static List<String> list = new ArrayList();

    public String createUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    @Override
    public void run() {

        synchronized (this) {
            if (!bol) {
                String uuid = createUUID();
                if (StringUtils.isEmpty(uuid)) {
                    logger.info("UUID IS NULL");
                }
                list.add(uuid);
                bol = true;

                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        logger.info("并发数量为" + number.intValue());
    }

}
