package cn.platform.agent.smp.DesHelperTest;

import cn.platform.agent.frame.core.helper.DesHelper;
import cn.platform.agent.smp.constant.ApplicationConstant;
import org.junit.Test;

/**
 * Created by SIVEN on 17/5/13.
 */
public class DesTest {

    @Test
    public void encrypt() throws Exception {

        DesHelper des = new DesHelper(ApplicationConstant.DES_PASSWORD);
        String password = des.encrypt("666666");
        System.out.println(password);

    }
}
