package cn.platform.agent.smp.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;

public class User {

    /**
     * uuid :
     */
    @NotNull(message = "uuid.must.notnull")
    @Size(max = 32, message = "uuid.size.beyond.error")
    private String uuid;

    /**
     * account : 帐号
     */
    @NotNull(message = "account.must.notnull")
    @Size(max = 20, message = "account.size.beyond.error")
    private String account;

    /**
     * password : 密码
     */
    @NotNull(message = "password.must.notnull")
    @Size(max = 100, message = "password.size.beyond.error")
    private String password;

    /**
     * name : 姓名
     */
    @Size(max = 20, message = "name.size.beyond.error")
    private String name;

    /**
     * mobile : 手机号码
     */
    @Size(max = 30, message = "mobile.size.beyond.error")
    private String mobile;

    /**
     * email : 邮箱
     */
    @Size(max = 50, message = "email.size.beyond.error")
    private String email;

    /**
     * createTime : 创建时间
     */
    private Date createTime;

    /**
     * status : 状态; Y有效, N禁用
     */
    @NotNull(message = "status.must.notnull")
    @Size(max = 1, message = "status.size.beyond.error")
    @Pattern(regexp = "(Y|N)" ,message = "status.must.bevalue")
    private String status;

    /**
     * mark : 备注
     */
    @Size(max = 255, message = "mark.size.beyond.error")
    private String mark;

    /**
     * telephone : 办公电话
     */
    @Size(max = 50, message = "telephone.size.beyond.error")
    private String telephone;

    /**
     * gender : 性别(1男, 2女)
     */
    @Size(max = 1, message = "gender.size.beyond.error")
    private String gender;

    /**
     * fax : 传真号
     */
    @Size(max = 20, message = "fax.size.beyond.error")
    private String fax;

    /**
     * code : 员工编码
     */
    @Size(max = 20, message = "code.size.beyond.error")
    private String code;

    private Date lastLoginTime;
    private String lastLoginIp;

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    public User() {
        super();
    }

    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getAccount() {
        return this.account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMark() {
        return this.mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getTelephone() {
        return this.telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFax() {
        return this.fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    @Override
    public String toString() {
        return String.format(
                "User[uuid='%s',account='%s',password='%s',name='%s',mobile='%s',email='%s',createTime='%s',status='%s',mark='%s',telephone='%s',gender='%s',fax='%s',code='%s',lastLoginIp='%s' , lastLoginTime='%s']", uuid, account, password, name, mobile, email, createTime, status, mark, telephone, gender, fax, code , lastLoginIp , lastLoginTime);
    }

}