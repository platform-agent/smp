package cn.platform.agent.smp.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;

public class Group {

    /**
     * uuid : uuid
     */
    @NotNull(message = "uuid.must.notnull")
    @Size(max = 32, message = "uuid.size.beyond.error")
    private String uuid;

    /**
     * code : 部门编码
     */
    @NotNull(message = "group.code.must.notnull")
    @Size(max = 20, message = "group.code.size.beyond.error")
    private String code;

    /**
     * name : 部门名称
     */
    @NotNull(message = "group.name.must.notnull")
    @Size(max = 30, message = "group.name.size.beyond.error")
    private String name;

    /**
     * puuid : 上级ID
     */
    @NotNull(message = "group.puuid.must.notnull")
    @Size(max = 32, message = "group.puuid.size.beyond.error")
    private String puuid;

    private String parentName;

    /**
     * status : 状态; Y有效, N禁用
     */
    @NotNull(message = "status.must.notnull")
    @Size(max = 1, message = "status.size.beyond.error")
    @Pattern(regexp = "(Y|N)" ,message = "status.must.bevalue")
    private String status;

    /**
     * sort : 排序
     */
    private Integer sort;

    /**
     * description : 描述
     */
    @Size(max = 255, message = "description.size.beyond.error")
    private String description;

    /**
     * createTime :
     */
    private Date createTime;

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public Group() {
        super();
    }

    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPuuid() {
        return puuid;
    }

    public void setPuuid(String puuid) {
        this.puuid = puuid;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getSort() {
        return this.sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    @Override
    public String toString() {
        return String.format(
                "Group[uuid='%s',code='%s',name='%s',puuid='%s',status='%s',sort='%s',description='%s',createTime='%s',]", uuid, code, name, puuid, status, sort, description, createTime);
    }

}