package cn.platform.agent.smp.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

public class Menu {

		/**
	* uuid :  
	*/
	@NotNull(message = "uuid.must.notnull")
	@Size(max = 32, message = "uuid.size.beyond.error")
	private String uuid;

	/**
	* name : 菜单名称 
	*/
	@NotNull(message = "name.must.notnull")
	@Size(max = 50, message = "name.size.beyond.error")
	private String name;

	/**
	* url : 菜单URL 
	*/
	@NotNull(message = "url.must.notnull")
	@Size(max = 80, message = "url.size.beyond.error")
	private String url;

	/**
	* status : 状态; Y有效, N禁用 
	*/
	@NotNull(message = "status.must.notnull")
	@Size(max = 1, message = "status.size.beyond.error")
	private String status;

	/**
	* puuid : 菜单父ID 
	*/
	@Size(max = 32, message = "puuid.size.beyond.error")
	private String puuid;

	/**
	* createTime : 创建时间 
	*/
	private Date createTime;

	/**
	* openMode : 打开方式 
	*/
	@Size(max = 10, message = "openMode.size.beyond.error")
	private String openMode;

	/**
	* description : 描述信息 
	*/
	@Size(max = 255, message = "description.size.beyond.error")
	private String description;

	/**
	* icon : 图标 
	*/
	@Size(max = 30, message = "icon.size.beyond.error")
	private String icon;



	public Menu() {
		super();
	}

		public String getUuid(){
		return this.uuid;
	}

	public void setUuid(String uuid){
		this.uuid=uuid;
	}

	public String getName(){
		return this.name;
	}

	public void setName(String name){
		this.name=name;
	}

	public String getUrl(){
		return this.url;
	}

	public void setUrl(String url){
		this.url=url;
	}

	public String getStatus(){
		return this.status;
	}

	public void setStatus(String status){
		this.status=status;
	}

	public String getPuuid(){
		return this.puuid;
	}

	public void setPuuid(String puuid){
		this.puuid=puuid;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime=createTime;
	}

	public String getOpenMode(){
		return this.openMode;
	}

	public void setOpenMode(String openMode){
		this.openMode=openMode;
	}

	public String getDescription(){
		return this.description;
	}

	public void setDescription(String description){
		this.description=description;
	}

	public String getIcon(){
		return this.icon;
	}

	public void setIcon(String icon){
		this.icon=icon;
	}



	@Override
	public String toString() {
		return String.format(
				"Menu[uuid='%s',name='%s',url='%s',status='%s',puuid='%s',createTime='%s',openMode='%s',description='%s',icon='%s',]",uuid,name,url,status,puuid,createTime,openMode,description,icon);
	}

}