package cn.platform.agent.smp.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

public class Role {

		/**
	* uuid : id 
	*/
	@NotNull(message = "uuid.must.notnull")
	@Size(max = 32, message = "uuid.size.beyond.error")
	private String uuid;

	/**
	* code : 角色编码 
	*/
	@NotNull(message = "code.must.notnull")
	@Size(max = 20, message = "code.size.beyond.error")
	private String code;

	/**
	* name : 角色名称 
	*/
	@NotNull(message = "name.must.notnull")
	@Size(max = 30, message = "name.size.beyond.error")
	private String name;

	/**
	* status : 状态; Y有效, N禁用 
	*/
	@NotNull(message = "status.must.notnull")
	@Size(max = 1, message = "status.size.beyond.error")
	private String status;

	/**
	* description : 描述 
	*/
	@Size(max = 255, message = "description.size.beyond.error")
	private String description;

	/**
	* createTime : 创建时间 
	*/
	private Date createTime;



	public Role() {
		super();
	}

		public String getUuid(){
		return this.uuid;
	}

	public void setUuid(String uuid){
		this.uuid=uuid;
	}

	public String getCode(){
		return this.code;
	}

	public void setCode(String code){
		this.code=code;
	}

	public String getName(){
		return this.name;
	}

	public void setName(String name){
		this.name=name;
	}

	public String getStatus(){
		return this.status;
	}

	public void setStatus(String status){
		this.status=status;
	}

	public String getDescription(){
		return this.description;
	}

	public void setDescription(String description){
		this.description=description;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setCreateTime(Date createTime){
		this.createTime=createTime;
	}



	@Override
	public String toString() {
		return String.format(
				"Role[uuid='%s',code='%s',name='%s',status='%s',description='%s',createTime='%s',]",uuid,code,name,status,description,createTime);
	}

}