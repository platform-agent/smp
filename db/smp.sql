
CREATE TABLE `sys_user` (
`uuid` varchar(32) NOT NULL,
`account` varchar(20) NOT NULL COMMENT '帐号',
`password` varchar(100) NOT NULL COMMENT '密码',
`name` varchar(20) NULL COMMENT '姓名',
`mobile` varchar(30) NULL COMMENT '手机号码',
`email` varchar(50) NULL COMMENT '邮箱',
`create_time` datetime NULL COMMENT '创建时间',
`status` char(1) NOT NULL DEFAULT Y COMMENT '状态; Y有效, N禁用',
`mark` varchar(255) NULL COMMENT '备注',
`telephone` varchar(50) NULL COMMENT '办公电话',
`gender` varchar(1) NULL DEFAULT 1 COMMENT '性别(1男, 2女)',
`fax` varchar(20) NULL COMMENT '传真号',
`code` varchar(20) NULL COMMENT '员工编码',
PRIMARY KEY (`uuid`) 
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = '系统用户';

CREATE TABLE `sys_group` (
`uuid` varchar(32) NOT NULL COMMENT 'id',
`code` varchar(20) NOT NULL COMMENT '部门编码',
`name` varchar(30) NOT NULL COMMENT '部门名称',
`parent_uuid` int(11) NOT NULL DEFAULT 0 COMMENT '上级ID',
`status` char(1) NOT NULL DEFAULT Y COMMENT '状态; Y有效, N禁用',
`sort` int(5) NULL DEFAULT 1 COMMENT '排序',
`description` varchar(255) NULL COMMENT '描述',
`create_time` datetime NULL,
PRIMARY KEY (`uuid`) 
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = '系统组';

CREATE TABLE `sys_role` (
`uuid` varchar(32) NOT NULL COMMENT 'id',
`code` varchar(20) NOT NULL COMMENT '角色编码',
`name` varchar(30) NOT NULL COMMENT '角色名称',
`status` CHAR(1) NOT NULL DEFAULT Y COMMENT '状态; Y有效, N禁用',
`description` varchar(255) NULL COMMENT '描述',
`create_time` datetime NULL COMMENT '创建时间',
PRIMARY KEY (`uuid`) 
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = '系统角色';

CREATE TABLE `sys_user_login` (
`uuid` varchar(32) NOT NULL,
`user_id` varchar(32) NULL COMMENT '用户ID',
`create_time` datetime NULL COMMENT '登录时间',
`ip` varchar(255) NULL COMMENT '登录IP',
PRIMARY KEY (`uuid`) 
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = '用户登录记录表';

CREATE TABLE `sys_menu` (
`uuid` varchar(32) NOT NULL,
`name` varchar(50) NOT NULL COMMENT '菜单名称',
`url` varchar(80) NOT NULL COMMENT '菜单URL',
`status` char(1) NOT NULL DEFAULT Y COMMENT '状态; Y有效, N禁用',
`parent_uuid` varchar(32) NULL DEFAULT 0 COMMENT '菜单父ID',
`create_time` datetime NULL COMMENT '创建时间',
`open_mode` varchar(10) NULL COMMENT '打开方式',
`description` varchar(255) NULL COMMENT '描述信息',
PRIMARY KEY (`uuid`) 
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = '系统菜单';

CREATE TABLE `sys_role_menu` (
`role_uuid` varchar(32) NOT NULL COMMENT '角色ID',
`menu_uuid` varchar(32) NOT NULL COMMENT '菜单ID',
`create_time` datetime NOT NULL,
`status` CHAR(1) NOT NULL DEFAULT Y COMMENT '状态; Y有效, N禁用',
PRIMARY KEY (`role_uuid`, `menu_uuid`) 
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = '角色菜单关联表';

CREATE TABLE `sys_user_role` (
`user_uuid` varchar(32) NOT NULL COMMENT '角色ID',
`role_uuid` varchar(32) NOT NULL COMMENT '菜单ID',
`create_time` datetime NOT NULL,
`status` CHAR(1) NOT NULL DEFAULT Y COMMENT '状态; Y有效, N禁用',
PRIMARY KEY (`user_uuid`, `role_uuid`) 
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = '用户角色关联表';

CREATE TABLE `sys_log` (
`uuid` varchar(32) NOT NULL,
`account` varchar(20) NULL COMMENT '帐号',
`create_time` datetime NULL COMMENT '操作时间',
`ip` varchar(20) NULL COMMENT '登录IP',
`operate` varchar(255) NULL COMMENT '操作',
`flag` char(1) NULL DEFAULT Y COMMENT '是否成功(Y是, N否)',
PRIMARY KEY (`uuid`) 
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = '系统安全日志';

